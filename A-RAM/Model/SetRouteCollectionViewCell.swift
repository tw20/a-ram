//
//  SetRouteCollectionViewCell.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 4/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit

class SetRouteCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var attractionNameLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var imageView: UIView!
    @IBOutlet weak var overlayView: UIView!
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        overlayView.layer.shadowRadius = 2.0
//        overlayView.layer.shadowOpacity = 0.8
//        overlayView.layer.cornerRadius = 15
//        overlayView.setGradient(topColour: UIColor(red:0.35, green:0.31, blue:0.27, alpha:1.0), bottomColour: UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.0)
//        )
//        self.clipsToBounds = false
//    }
}

extension UIView {
    func setGradient(topColour: UIColor, bottomColour: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [topColour.cgColor, bottomColour.cgColor]
        //gradientLayer.locations = [0.0, 1.0]
        //gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        //gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
}

