//
//  CollectionViewCell.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 1/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var attractionLabel: UILabel!
    @IBOutlet weak var attractionLogoImageView: UIImageView!
    
    
    
}
