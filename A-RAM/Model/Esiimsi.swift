//
//  Esiimsi.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 2/12/2562 BE.
//  Copyright © 2562 Pakkapong Somboon. All rights reserved.
//

import Foundation

class Esiimsi {
    var number:Int
    var prediction:String
    
    init(number: Int, prediction: String) {
        self.number = number
        self.prediction = prediction
    }
}
