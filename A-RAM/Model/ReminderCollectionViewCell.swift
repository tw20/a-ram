//
//  ReminderCollectionViewCell.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 3/3/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit

class ReminderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var eventnameLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dayView: UIView!
    @IBOutlet weak var bgImageView: UIImageView!
}
