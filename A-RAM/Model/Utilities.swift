//
//  Utilities.swift
//  customauth
//
//  Created by Christopher Ching on 2019-05-09.
//  Copyright © 2019 Christopher Ching. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import AVKit
import AVFoundation

class Utilities {
    
    
    
    static func styleTextField(_ textfield:UITextField) {
        
        // Create the bottom line
        let bottomLine = CALayer()
        
        bottomLine.frame = CGRect(x: 0, y: textfield.frame.height - 2, width: textfield.frame.width, height: 1)
        
        bottomLine.backgroundColor = UIColor.init(red:0.47, green:0.54, blue:0.51, alpha:1.0).cgColor
        
        // Remove border on text field
        textfield.borderStyle = .none
        
        
        
        // Add the line to the text field
        textfield.layer.addSublayer(bottomLine)
        
    }
    
    static func styleFilledButton(_ button:UIButton) {
        
        // Filled rounded corner style
        button.backgroundColor = UIColor.init(red: 48/255, green: 173/255, blue: 99/255, alpha: 1)
        button.layer.cornerRadius = 25.0
        button.tintColor = UIColor.white
    }
    
    static func styleHollowButton(_ button:UIButton) {
        
        // Hollow rounded corner style
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.cornerRadius = 25.0
        button.tintColor = UIColor.black
    }
    
    static func isPasswordValid(_ password : String) -> Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$")
        return passwordTest.evaluate(with: password)
    }
    
}

struct AppUtility {

    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {

        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }

    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {

        self.lockOrientation(orientation)

        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }

}

extension UIViewController {
    func handlerFireAuthError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            let alert = UIAlertController(title: "Error", message: errorCode.errorMessage, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    struct Keys {
        static let email = "Email"
        static let username = "Username"
        static let birth = "Birthdate"
        static let gender = "Gender"
        static let zodiac = "Zodiac"
        static let route = "Route"
        static let icon = "Route_logo"
    }
    
    func saveDataToUserDefaults(email: String, username: String, birthdate: Date, gender: String, zodiac: String) {
        let defaults = UserDefaults.standard
        defaults.set(email, forKey: Keys.email)
        defaults.set(username, forKey: Keys.username)
        defaults.set(birthdate, forKey: Keys.birth)
        defaults.set(gender, forKey: Keys.gender)
        defaults.set(zodiac, forKey: Keys.zodiac)
        print("\(defaults.string(forKey: Keys.email)) , \(defaults.string(forKey: Keys.username)) , \(defaults.object(forKey: Keys.birth) as! Date) , \(String(describing: defaults.string(forKey: Keys.gender))) , \(defaults.string(forKey: Keys.zodiac))")
    }
    
    func saveRouteToUserDefaults(route: [String], icon: [Int]) {
        let defaults = UserDefaults.standard
        defaults.set(route, forKey: Keys.route)
        defaults.set(icon, forKey: Keys.icon)
        print("\(defaults.object(forKey: Keys.route)) , \(defaults.object(forKey: Keys.icon))")
    }
    
    func deleteRouteFromUserDefaults() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: Keys.route)
        defaults.removeObject(forKey: Keys.icon)
    }
//    func getDataFromUserDefaults(email: String, username: String, birthdate: Date, gender: String, zodiac: String) {
//        let defaults = UserDefaults.standard
//        let getEmail = defaults.string(forKey: Keys.email)
//        let getUsername = defaults.string(forKey: Keys.username)
//        let getBirthdate = defaults.object(forKey: Keys.birth) as! Date
//        let getGender = defaults.string(forKey: Keys.gender)
//        let getZodiac = defaults.string(forKey: Keys.zodiac)
//    }
    
    func deleteDataFromUserDefaults() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: Keys.email)
        defaults.removeObject(forKey: Keys.username)
        defaults.removeObject(forKey: Keys.birth)
        defaults.removeObject(forKey: Keys.gender)
        defaults.removeObject(forKey: Keys.zodiac)
        defaults.removeObject(forKey: Keys.route)
        defaults.removeObject(forKey: Keys.icon)
    }
    
    func addDissmissSwipe() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(swipe:)))
        leftSwipe.direction = UISwipeGestureRecognizer.Direction.right
        view.addGestureRecognizer(leftSwipe)
    }
    
    @objc func swipeAction(swipe: UISwipeGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    func addBackNavSwipe() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(navSwipeAction(swipe:)))
        leftSwipe.direction = UISwipeGestureRecognizer.Direction.right
        view.addGestureRecognizer(leftSwipe)
    }
    
    @objc func navSwipeAction(swipe: UISwipeGestureRecognizer) {
        navigationController?.popViewController(animated: true)
    }
    
    func addDissmissTap() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func setUpLoading(logo: UIView) {
        let loading = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        loading.backgroundColor = .white
        logo.center = loading.center
        setUpLoadingVideo(logo: logo)
        view.addSubview(loading)
        loading.addSubview(logo)
    }
    
    func setUpLoadingVideo(logo: UIView) {
        let path = URL(fileURLWithPath: Bundle.main.path(forResource: "Loading", ofType: "mp4")!)
        let player = AVPlayer(url: path)
        
        let newPlayer = AVPlayerLayer(player: player)
        newPlayer.frame = logo.frame
        logo.layer.addSublayer(newPlayer)
        newPlayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        player.play()
        player.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        NotificationCenter.default.addObserver(self, selector: #selector(Utilities.videoDidPlayToEnd(_:)), name: NSNotification.Name("AVPlayerItemDidPlayToEndTimeNotification"), object: player.currentItem)
    }
    
}

extension AuthErrorCode {
    var errorMessage: String {
        switch self {
        case .emailAlreadyInUse:
            return "This email is already in use with another account."
            
        case .userNotFound:
            return "Account not found for the specified user. Please check and try again."
            
        case .userDisabled:
            return "Your account has been disabled. Please contact support."
            
        case .invalidEmail, .invalidSender, .invalidRecipientEmail:
            return "Please enter a valid email."
            
        case .networkError:
            return "Network error. Please try again."
            
        case .weakPassword:
            return "Your password is too weak. The password must be 6 characters long or more."
            
        case .wrongPassword:
            return "Your password or email is incorrect."
            
        default:
            return "Something went wrong. Please try again."
        }
    }
}

extension Utilities {
    @objc func videoDidPlayToEnd(_ notification: Notification) {
        let player: AVPlayerItem = notification.object as! AVPlayerItem
        player.seek(to: CMTime.zero, completionHandler: nil)
    }
}

extension UIViewController {
    func showSpriner(time: Double) {
        var aView = UIView(frame: self.view.bounds)
        aView.backgroundColor = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)

        let ai = UIActivityIndicatorView(style: .large)
        ai.center = aView.center
        ai.startAnimating()
        self.navigationController?.navigationBar.layer.zPosition = -1;
        aView.addSubview(ai)
        view.addSubview(aView)
        Timer.scheduledTimer(withTimeInterval: time, repeats: false) { (t) in
            aView.removeFromSuperview()
        }
    }
}


