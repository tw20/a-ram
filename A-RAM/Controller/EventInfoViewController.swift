//
//  EventInfoViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 21/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase
import EventKit

class EventInfoViewController: UIViewController {
    
    @IBOutlet weak var remindBtn: UIButton!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var startDayLabel: UILabel!
    @IBOutlet weak var startMonthLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var eventInfoTextView: UITextView!
    @IBOutlet weak var eventNameLabel: UILabel!
    
    var eventName: String = ""
    var eventInfo:String = ""
    var start: Date?
    var end: Date?
    let db = Firestore.firestore()
    let dateFormatter = DateFormatter()
    let user = Auth.auth().currentUser!
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
        getInfo()
        // Do any additional setup after loading the view.
    }
    
    func setUpView() {
        showSpriner(time: 1)
        infoView.roundCorners(cornerRadius: 15)
        remindBtn.layer.cornerRadius = 25
        eventNameLabel.text = eventName
        addDissmissSwipe()
    }
    
    func getInfo() {
        db.collection("event").whereField("name", isEqualTo: eventName).getDocuments { (snapshot, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }else {
                for document in snapshot!.documents {
                    self.eventInfo = document.data()["information"] as! String
                    self.start = (document.data()["start"] as! Timestamp).dateValue()
                    self.end = (document.data()["end"] as! Timestamp).dateValue()
                    
                    self.eventInfoTextView.text = self.eventInfo
                    // convert time
                    let dayFormatter = self.dateFormatter
                    dayFormatter.dateFormat = "d"
                    dayFormatter.timeZone = TimeZone(abbreviation: "GMT+7")
                    dayFormatter.locale = NSLocale.current
                    self.startDayLabel.text = dayFormatter.string(from: self.start!)
                    let monthFormatter = self.dateFormatter
                    monthFormatter.dateFormat = "MMMM"
                    monthFormatter.timeZone = TimeZone(abbreviation: "GMT+7")
                    monthFormatter.locale = NSLocale.current
                    self.startMonthLabel.text = monthFormatter.string(from: self.start!)
                    let timeFormatter = self.dateFormatter
                    timeFormatter.dateFormat = "h:mma"
                    timeFormatter.timeZone = TimeZone(abbreviation: "GMT+7")
                    timeFormatter.locale = NSLocale.current
                    self.startTimeLabel.text = timeFormatter.string(from: self.start!)
                    self.endTimeLabel.text = timeFormatter.string(from: self.end!)
                    
                }
            }
            self.checkReminder()
        }
    }
    
    func checkReminder() {
        let eventStore: EKEventStore = EKEventStore()
        var eventAlreadyExists = false
        let event = EKEvent(eventStore: eventStore)
        event.title = eventName
        event.startDate = start
        event.endDate = end
        event.calendar = eventStore.defaultCalendarForNewEvents

        let predicate = eventStore.predicateForEvents(withStart: start!, end: end!, calendars: nil)
        let existingEvents = eventStore.events(matching: predicate)

        eventAlreadyExists = existingEvents.contains(where: {event in eventName == event.title && event.startDate == start && event.endDate == end})

        // Matching event found, don't add it again, just display alert
        if eventAlreadyExists {
            self.remindBtn.setTitle("REMINDED", for: .disabled)
            self.remindBtn.backgroundColor = UIColor(red:0.71, green:0.71, blue:0.71, alpha:1.0)
            self.remindBtn.isEnabled = false
        }
    }
    
    @IBAction func remindTapped(_ sender: Any) {
        let eventStore: EKEventStore = EKEventStore()
        eventStore.requestAccess(to: .event) { (grant, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }else {
                let event: EKEvent = EKEvent(eventStore: eventStore)
                event.title = "\(self.eventName)"
                event.startDate = self.start!
                event.endDate = self.end
                event.notes = "\(self.eventInfo)"
                let alarm = EKAlarm(relativeOffset: -86400)
                event.alarms = [alarm]
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let error as NSError {
                    print("Error adding event: \(error)")
                }
                self.db.collection("user").document(self.user.uid).collection("reminder").addDocument(data: ["name" : self.eventName, "information": self.eventInfo, "start": self.start!, "end": self.end!])
                DispatchQueue.main.async
                {
                    self.remindBtn.setTitle("REMINDED", for: .disabled)
                    self.remindBtn.backgroundColor = UIColor(red:0.71, green:0.71, blue:0.71, alpha:1.0)
                    self.remindBtn.isEnabled = false
                }
            }
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
