//
//  MeditationViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 19/1/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MeditationViewController: UIViewController {

    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var timerBtn: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    var timer = Timer()
    var counter = 10
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
        
        // Do any additional setup after loading the view.
    }
    
    func setUpView() {
        infoView.layer.cornerRadius = 15
        continueBtn.layer.cornerRadius = 25
        continueBtn.isHidden = true
        continueBtn.alpha = 0
    }

    @IBAction func timerTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.timerBtn.alpha = 0
            self.startLabel.alpha = 0
        }
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction() {
        counter -= 1
        timerLabel.text = "\(counter)"
        if counter == 0 {
            timerLabel.text = "0"
            timer.invalidate()
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            continueBtn.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.continueBtn.alpha = 1
            }
        }
    }
    
    @IBAction func continueTapped(_ sender: Any) {
        performSegue(withIdentifier: "endActivity", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! AlertViewController
        UIView.animate(withDuration: 0.5, delay: 1, options: .curveEaseIn, animations: {
        }) { (success:Bool) in
            vc.endLogo = "meditation_logo"
            vc.nextLogo = "tree_logo"
            vc.nameLabel.text = "THE ORDINATION HALL"
            vc.attractionLabel.text = "SRI MAHA BODHI TREE"
            vc.endAlertIn()
        }
    }
    
}
