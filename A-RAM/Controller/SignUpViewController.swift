//
//  SignUpViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 8/12/2562 BE.
//  Copyright © 2562 Pakkapong Somboon. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import AVKit
import AVFoundation

class SignUpViewController: UIViewController {
    
    @IBOutlet var logoView: UIView!
    @IBOutlet var loadingView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var birthdateTextField: UITextField!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    private var datepicker: UIDatePicker?
    var selectedGender: String?
    var genderArray = ["Male", "Female", "Other"]
    let db = Firestore.firestore()
    let calendar = Calendar.current
    let dateFormatter = DateFormatter()
    let zodiac: [String] = ["Monkey","Cock","Dog","Pig","Rat","Cow","Tiger","Rabbit","Great Snake","Snake","Horse","Goat"]
    var zodiacYear: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpElement()
        createPickerView()
        createDatePicker()

        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        
        // Do any additional setup after loading the view.
    }
    
    func setUpElement() {
        navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleTextField(usernameTextField)
        Utilities.styleTextField(genderTextField)
        Utilities.styleTextField(birthdateTextField)
        birthdateTextField.inputView = datepicker
        signUpBtn.layer.cornerRadius = 25
        //errorLabel.alpha = 0
        addBackNavSwipe()
        addDissmissTap()
    }
    
    func setUpLoading() {
        loadingView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        loadingView.backgroundColor = .white
        logoView.center = loadingView.center
        setUpVideo()
        self.navigationController?.navigationBar.layer.zPosition = -1;
        self.view.addSubview(loadingView)
        self.loadingView.addSubview(logoView)
    }

    private func setUpVideo() {
        let path = URL(fileURLWithPath: Bundle.main.path(forResource: "Loading", ofType: "mp4")!)
        let player = AVPlayer(url: path)

        let newPlayer = AVPlayerLayer(player: player)
        newPlayer.frame = self.logoView.frame
        self.logoView.layer.addSublayer(newPlayer)
        newPlayer.videoGravity = AVLayerVideoGravity.resizeAspectFill

        player.play()
        player.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.videoDidPlayToEnd(_:)), name: NSNotification.Name("AVPlayerItemDidPlayToEndTimeNotification"), object: player.currentItem)

    }

    @objc func videoDidPlayToEnd(_ notification: Notification) {
        let player: AVPlayerItem = notification.object as! AVPlayerItem
        player.seek(to: CMTime.zero, completionHandler: nil)
    }
    
    func validateField() -> String? {
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            usernameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            genderTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            birthdateTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill in all fields"
        }
        let cleannedPassword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilities.isPasswordValid(cleannedPassword) == false {
            // password not secure
            return "Please make sure your password is at least 8 characters, contains a special charater and number"
        }
            return nil
    }
    
    
    @IBAction func signUpTapped(_ sender: Any) {
        setUpLoading(logo: logoView)
        let error = validateField()
        if error != nil {
            //showError(error!)
        } else {
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let username = usernameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let gender = genderTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let unixTimestamp = self.datepicker?.date.timeIntervalSince1970
            let birthdate = Date(timeIntervalSince1970: unixTimestamp!)
            let yearFormatter = self.dateFormatter
            yearFormatter.dateFormat = "YYYY"
            let birthYear = Int(yearFormatter.string(from: birthdate))!
            
            for index in 0...11 {
                let converter = birthYear % 12
                if converter == index {
                    zodiacYear = zodiac[index]
                }
            }
            //let credential = EmailAuthProvider.credential(withEmail: email, password: password)
            //guard let user = Auth.auth().currentUser else { return }
            Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                if let error = error {
                    //self.showError("Error creating user")
                    self.handlerFireAuthError(error)
                    debugPrint(error.localizedDescription)
                }else {
                        self.db.collection("user").document(result!.user.uid).setData([
                                                    "email" : email,"password": password, "username": username, "gender": gender, "birthdate": birthdate, "zodiac": self.zodiacYear, "uid": result!.user.uid]) { (error) in
                                                        if let error = error {
                                                            debugPrint(error.localizedDescription)
                                                        } else {
                                                            self.saveDataToUserDefaults(email: email, username: username, birthdate: birthdate, gender: gender, zodiac: self.zodiacYear)
                                                            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                                                                            if let error = error {
                                                                                //self.showError("Error creating user")
                                                                                self.handlerFireAuthError(error)
                                                                                debugPrint(error.localizedDescription)
                                                                            }else {
                                                                                    self.performSegue(withIdentifier: "zodiacSegue", sender: self)
                                                            //                        let storyboard = UIStoryboard(name: "Login", bundle: nil)
                                                            //                        let controller = storyboard.instantiateViewController(identifier: "zodiacVC")
                                                            //                        self.present(controller, animated: true, completion: nil)
                                                                            }
                                                            }
                                                        }
                                                }
                }
            }
                
                
        }
    }
    
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ZodiacNavigationViewController
        vc.zodiac = zodiacYear
    }
}

extension SignUpViewController: UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedGender = genderArray[row]
        genderTextField.text = selectedGender
    }
    
    func createPickerView() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        
        genderTextField.inputView = pickerView
    }
    
    func createDatePicker() {
        datepicker = UIDatePicker()
        datepicker?.datePickerMode = .date
        datepicker?.addTarget(self, action: #selector(SignUpViewController.dateChanged(datePicker:)), for: .valueChanged)
        
        birthdateTextField.inputView = datepicker
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+7")
        dateFormatter.locale = NSLocale.current
        birthdateTextField.text = dateFormatter.string(from: datePicker.date)
        
    }
}
