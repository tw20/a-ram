//
//  EditProfileViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 21/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var birthdateTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var profileImageView: UIImageView!
    
    private var datepicker: UIDatePicker?
    var selectedGender: String?
    var genderArray = ["Male", "Female", "Other"]
    let db = Firestore.firestore()
    let user = Auth.auth().currentUser!
    var email: String = ""
    var username: String = ""
    var gender: String = ""
    var birthdate: Date?
    let zodiac: [String] = ["Monkey","Cock","Dog","Pig","Rat","Cow","Tiger","Rabbit","Great Snake","Snake","Horse","Goat"]
    var zodiacYear: String = ""
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getInfo()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    func setUpView() {
        showSpriner(time: 1)
        saveBtn.layer.cornerRadius = 25
        headerView.bottomRoundCorners(cornerRadius: 15)
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(usernameTextField)
        Utilities.styleTextField(genderTextField)
        Utilities.styleTextField(birthdateTextField)
        createGenderPickerView()
        createDatePicker()
        addBackNavSwipe()
        addDissmissTap()
    }
    
    func getInfo() {
        db.collection("user").document(user.uid).getDocument { (document, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }else {
                self.email = document?.data()!["email"] as! String
                self.username = document?.data()!["username"] as! String
                self.gender = document?.data()!["gender"] as! String
                self.birthdate = (document?.data()!["birthdate"] as! Timestamp).dateValue()
                self.zodiacYear = document?.data()!["zodiac"] as! String
                
                self.emailTextField.text = self.email
                self.usernameTextField.text = self.username
                self.genderTextField.text = self.gender
                self.selectedGender = self.gender
                let birthFormatter = self.dateFormatter
                birthFormatter.dateFormat = "MM/dd/yyyy"
                birthFormatter.timeZone = TimeZone(abbreviation: "GMT+7")
                birthFormatter.locale = NSLocale.current
                self.birthdateTextField.text = birthFormatter.string(from: self.birthdate!)
                self.datepicker?.date = self.birthdate!
                self.profileImageView.image = UIImage(named: self.zodiacYear)
                
            }
        }
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let username = usernameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let gender = genderTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let unixTimestamp = self.datepicker?.date.timeIntervalSince1970
        let birthdate = Date(timeIntervalSince1970: unixTimestamp!)
        let yearFormatter = self.dateFormatter
        yearFormatter.dateFormat = "YYYY"
        let birthYear = Int(yearFormatter.string(from: birthdate))!
        
        for index in 0...11 {
            let converter = birthYear % 12
            if converter == index {
                zodiacYear = zodiac[index]
            }
        }
        db.collection("user").document(user.uid).updateData([
            "email" : email, "username": username, "gender": gender, "birthdate": birthdate, "zodiac": self.zodiacYear]) { (error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                }else {
                    self.deleteDataFromUserDefaults()
                    self.saveDataToUserDefaults(email: email, username: username, birthdate: birthdate, gender: gender, zodiac: self.zodiacYear)
                    self.navigationController?.popToRootViewController(animated: true)
                }
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}


extension EditProfileViewController: UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedGender = genderArray[row]
        genderTextField.text = selectedGender
    }
    
    func createGenderPickerView() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        genderTextField.inputView = pickerView
    }
    
    func createDatePicker() {
        datepicker = UIDatePicker()
        datepicker?.datePickerMode = .date
        datepicker?.addTarget(self, action: #selector(EditProfileViewController.dateChanged(datePicker:)), for: .valueChanged)
        
        birthdateTextField.inputView = datepicker
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = self.dateFormatter
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+7")
        dateFormatter.locale = NSLocale.current
        birthdateTextField.text = dateFormatter.string(from: datePicker.date)
        
    }
}
