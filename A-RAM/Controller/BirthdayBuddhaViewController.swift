//
//  BirthdayBuddhaViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 19/1/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase

class BirthdayBuddhaViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var postureInfoLabel: UILabel!
    @IBOutlet weak var postureLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var statueImageView: UIImageView!
    
    
    //var dateName = ""
    var birthdate = Date()
    var date = ""
    let dateFormatter = DateFormatter()
    let db = Firestore.firestore()
    let user = Auth.auth().currentUser!
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
        setUpInfo()
        print(postureInfoLabel.bounds.size.height)
        // Do any additional setup after loading the view.
    }
    
    func setUpView() {
        continueBtn.layer.cornerRadius = 25
        infoView.layer.cornerRadius = 15
        scrollView.contentInset = UIEdgeInsets(top: (scrollView.frame.height)/2.5, left: 0, bottom: 0, right: 0)
        //scrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height)
        //scrollView.autoresizingMask = .flexibleHeight
        birthdate = defaults.object(forKey: Keys.birth) as! Date
        dateFormatter.dateFormat = "EEEE"
        date = dateFormatter.string(from: birthdate)
        dayLabel.text = date
        statueImageView.image = UIImage(named: date)
        print(date)
    }
    
    func setUpInfo() {
        switch date {
        case "Monday":
            view.backgroundColor = UIColor(red:0.90, green:0.75, blue:0.51, alpha:1.0)
            postureLabel.text = "Standing Buddha posture statue"
            postureInfoLabel.text = "Pang Buddha image style up Because his father was a Buddhist relatives in Kapilavastu. His relatives and Buddha's mother was Devadaha who live on opposite sides of the river Orhini. Caused controversy vie with each other to bring water to the crop. Doreen troops at war ever. Buddha had come to try to stop people from quarreling negotiations is not to ban the slaughter relatives."
            
        case "Tuesday":
            view.backgroundColor = UIColor(red:0.84, green:0.60, blue:0.62, alpha:1.0)
            postureLabel.text = "Reclining posture statue"
            postureInfoLabel.text = "Reclining Buddha, or sometimes referred to as a death camp Buddha at the Buddha has said that the Dalai Jun Thera paved the seat down. During the nesting pair Then he sat playoff atmosphere in Bangkok Reclining Sihamoni. His heart did not rise. But please, Mr. Pat pan Pripachk a saint, the last coming off Khan's death. Those Buddhists together melancholy wailing lament of him. Ananda and Anuruddha not preach to placate the public. When Buddhists commemorate the death of His Highness. This has created the statue up. To worship the Buddha"
            
        case "Wednesday":
            view.backgroundColor = UIColor(red:0.40, green:0.45, blue:0.29, alpha:1.0)
            postureLabel.text = "Alms bowl posture statue"
            postureInfoLabel.text = "When the Buddha has manifested miracles prosperity. Flying up in the air in front of her family have. So that the elders had seen. And did obeisance at bigoted. Thus spoke the Sermon on the Vessantara Jataka. Then the relatives were dispersed by no one told me to beg bread at breakfast the next day. With mistook him as a son and as a priest I would have food prepared in the palace of his father. But Buddha monk disciples went back, take alms, pilgrimage to the highways in the city."
            
        case "Thursday":
            view.backgroundColor = UIColor(red:0.85, green:0.52, blue:0.16, alpha:1.0)
            postureLabel.text = "Meditation posture statue"
            postureInfoLabel.text = "Pang Pang Prince's enlightenment is perfected or Buddha sat cross-legged position on the bench grass. Maha Bodhi tree Near the river's old philosophy. And have gained enlightenment, the Buddha is Samopti perception. When the full moon of the 15th lunar month, 6 BC 45 years, which corresponded to the Vesak itself."
            
        case "Friday":
            view.backgroundColor = UIColor(red:0.36, green:0.47, blue:0.59, alpha:1.0)
            postureLabel.text = "Said posture statue"
            postureInfoLabel.text = "Shortly after the Enlightenment. Buddha was sitting under a banyan tree (palm Bosch's The Crow) said he was wrong to consider the Enlightenment as a fair resolution profound. Difficult to know as humanly possible. It will not discourage the teaching world. Who would have thought that a few people who can understand his religion. Hot Dames Shambdi prom was told to invite him to preach in this world. A person with passion is light enough not to have a fair hearing. That is why the Buddha called Pang said."
            
        case "Saturday":
            view.backgroundColor = UIColor(red:0.65, green:0.31, blue:0.36, alpha:1.0)
            postureLabel.text = "Narkporg posture statue"
            postureInfoLabel.text = "When Buddha attained enlightenment and stamp initiates discovering eat fielder joy resulting from freeing passion is in an area not far from the Bodhi Tree of the seventh day of the week at 3 this time and sat under a tree Mutchalin (the clubs) at that time. The rain has not stopped falling Naga its name Mutchalin serpent was the magic show into the seventh round, hairy cobra hood spread Buddha cover the prevent rain have foundered. White nine-tiered umbrella being a symbol of royalty alike unto the Lord thy sector."
            
        case "Sunday":
            view.backgroundColor = UIColor(red:0.79, green:0.24, blue:0.24, alpha:1.0)
            postureLabel.text = "Twuynet posture statue"
            postureInfoLabel.text = "When Royal Prophet has gained enlightenment Anutarasoti Samopti the perception then. He was sitting at meat Fielder (Deriving pleasure from peace) under the Bodhi Tree for a period of seven days, then went and stood at the door of the East Bodhi Tree. Bodhi Tree sees without blinking my eyes a 7-day period, which stood in this place has that name. Miss New Jedi to the present as a result of the creation of this statue called Pang popular Twaientr build a statue to worship the birth of Sunday."
            
        default:
            print(date)
        }
    }
    
    @IBAction func continueTapped(_ sender: Any) {
        performSegue(withIdentifier: "endActivity", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! AlertViewController
        UIView.animate(withDuration: 0.5, delay: 1, options: .curveEaseIn, animations: {
        }) { (success:Bool) in
            vc.nameLabel.text = "BIRTHDAY BUDDHA STATUE"
            vc.endLogo = "birth_logo"
            vc.nextLogo = "esiimsi_logo"
            vc.attractionLabel.text = "ESIIMSI"
            vc.endAlertIn()
        }
    }
    

}
