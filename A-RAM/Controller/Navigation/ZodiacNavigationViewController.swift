//
//  ZodiacNavigationViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 11/3/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit

class ZodiacNavigationViewController: UINavigationController {

    var zodiac: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//        let vc = self.viewControllers as! ZodiacViewController //first view controller in the tabbar
//        vc.zodiac = zodiac
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ZodiacViewController
        vc.zodiac = zodiac
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
