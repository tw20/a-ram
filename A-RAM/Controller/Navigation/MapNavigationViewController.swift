//
//  MapNavigationViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 11/3/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit

class MapNavigationViewController: UINavigationController {

    var route: [String] = []
    var number: [Int] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       let vc = segue.destination as! ViewController
       vc.name = route
       vc.attractionImage = number
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
