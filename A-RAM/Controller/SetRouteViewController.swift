//
//  SetRouteViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 4/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase
import AVKit
import AVFoundation

class SetRouteViewController: UIViewController {
    
    @IBOutlet weak var logoView: UIView!
    @IBOutlet var loadingView: UIView!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerView: UIView!
    
    var attractionName: [String] = []
    var attractionInfo: [String] = []
    var attractionImageUrl: [String] = []
    var attractionLogo: [UIImage] = [
        UIImage(named: "1")!,
        UIImage(named: "2")!,
        UIImage(named: "3")!,
        UIImage(named: "4")!,
        UIImage(named: "5")!,
        UIImage(named: "6")!,
        UIImage(named: "7")!,
        UIImage(named: "8")!,
        UIImage(named: "9")!
    ]
    var number = [1,2,3,4,5,6,7,8,9]
    
    var db: Firestore!
    let user = Auth.auth().currentUser
    
    override func loadView() {
        super.loadView()
        
        db = Firestore.firestore()
        getInfo()
        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        showSpriner(time: 2)
    }
    
    func setUpView() {
        headerView.bottomRoundCorners(cornerRadius: 15)
        startBtn.layer.cornerRadius = 25
    }
    
    func getInfo() {
        db.collection("attraction").getDocuments { (snapshot, error) in
            if let error = error {
                print(error)
            } else {
                for document in snapshot!.documents {
                    self.attractionName.append(document.data()["name"] as! String)
                    self.attractionInfo.append(document.data()["information"] as! String)
                    self.attractionImageUrl.append(document.data()["image"] as! String)
                }
            }
            Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (t) in
                self.collectionView.reloadData()
            }
            
            }
        }
        

    func addInfo(dataName: String, dataInfo: String, dataImage: String) {
        attractionName.append(dataName)
        attractionInfo.append(dataInfo)
        attractionImageUrl.append(dataImage)
        
        loadingView.removeFromSuperview()
    }
    
    func setUpLoading() {
        loadingView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        loadingView.backgroundColor = .white
        logoView.center = loadingView.center
        setUpVideo()
        self.view.addSubview(loadingView)
        self.loadingView.addSubview(logoView)
    }

    private func setUpVideo() {
        let path = URL(fileURLWithPath: Bundle.main.path(forResource: "Loading", ofType: "mp4")!)
        let player = AVPlayer(url: path)

        let newPlayer = AVPlayerLayer(player: player)
        newPlayer.frame = self.logoView.frame
        self.logoView.layer.addSublayer(newPlayer)
        newPlayer.videoGravity = AVLayerVideoGravity.resizeAspectFill

        player.play()
        player.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.videoDidPlayToEnd(_:)), name: NSNotification.Name("AVPlayerItemDidPlayToEndTimeNotification"), object: player.currentItem)

    }

    @objc func videoDidPlayToEnd(_ notification: Notification) {
        let player: AVPlayerItem = notification.object as! AVPlayerItem
        player.seek(to: CMTime.zero, completionHandler: nil)
    }
    
    
    func addRoute() {
        let count = attractionName.count - 1
        for index in 0...count {
            db.collection("user").document(user!.uid).collection("route").document("\((index+1))").setData(["number" : number[index], "name": attractionName[index], "isArcheived": false])
        }
        
    }
    
    @IBAction func startTapped(_ sender: Any) {
        addRoute()
        saveRouteToUserDefaults(route: attractionName, icon: number)
        //performSegue(withIdentifier: "startSegue", sender: nil)
    }
    
}

extension UIView {
    func bottomRoundCorners(cornerRadius: Double) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
}

//extension UIImageView {
//    public func imageFromUrl(urlString: String) {
//        if let url = URL(string: urlString) {
//            let request = URLRequest(url: url)
//            URLSession.sendAsynchronousRequest(request, queue: OperationQueue.mainQueue) {
//                (response: URLResponse?, data: NSData?, error: NSError?) -> Void in
//                if let imageData = data as NSData? {
//                    self.image = UIImage(data: imageData)
//                }
//            }
//        }
//    }
//}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

extension SetRouteViewController: UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attractionName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetRouteCell", for: indexPath) as! SetRouteCollectionViewCell
        let url = URL(string: attractionImageUrl[indexPath.item])
        cell.logoImageView.image = attractionLogo[indexPath.item]
        cell.attractionNameLabel.text = attractionName[indexPath.item]
        cell.infoLabel.text = attractionInfo[indexPath.item]
        cell.bgImageView.kf.setImage(with: url)
        cell.infoLabel.lineBreakMode = .byTruncatingTail
        cell.infoLabel.numberOfLines = 3
        cell.layer.cornerRadius = 15
        cell.imageView.layer.cornerRadius = 15
         //cell.overlayView.layer.cornerRadius = 15
        cell.bgImageView.layer.cornerRadius = 15
        cell.bgImageView.contentMode = .scaleAspectFill
         //cell.overlayView.layer.shadowRadius = 2.0
         //cell.overlayView.layer.shadowOpacity = 0.8
         //cell.overlayView.layer.masksToBounds = true
         //cell.overlayView.setGradient(topColour: UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0), bottomColour: UIColor(red:0.35, green:0.31, blue:0.27, alpha:0.0))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desVC = mainStoryboard.instantiateViewController(identifier: "attractionInfo") as! AttractionInfoViewController
        desVC.attractionName = attractionName[indexPath.item]
        desVC.logoImage = number[indexPath.item]
        self.present(desVC, animated: true, completion: nil)
    }
    
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        let layout = self.collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
//        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
//        
//        var offset = targetContentOffset.pointee
//        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
//        let roundedIndex = round(index)
//        
//        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing, y: -scrollView.contentInset.top)
//        targetContentOffset.pointee = offset
//    }
}
