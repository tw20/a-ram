//
//  TicketViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 19/1/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class TicketViewController: UIViewController {
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var ticketVideoView: UIView!
    var videoPlayer: AVPlayer?
    var videoPlayerLayer: AVPlayerLayer?
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
        setUpVideo()
        // Do any additional setup after loading the view.
    }
    func setUpView() {
        infoView.layer.cornerRadius = 15
        continueBtn.layer.cornerRadius = 25
        continueBtn.alpha = 0
        continueBtn.isHidden = true
    }
    
    func setUpVideo() {
        let bundlePath = Bundle.main.path(forResource: "Ticket", ofType: "mp4")
        guard bundlePath != nil else {
            return
        }
        
        let url = URL(fileURLWithPath: bundlePath!)
        let item = AVPlayerItem(url: url)
        videoPlayer = AVPlayer(playerItem: item)
        videoPlayerLayer = AVPlayerLayer(player: videoPlayer!)
        //videoPlayerLayer?.frame = self.ticketVideoView.frame
        videoPlayerLayer?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        view.layer.insertSublayer(videoPlayerLayer!, at: 0)
        
        
        videoPlayer?.playImmediately(atRate: 1)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.videoDidPlayToEnd(_:)), name: NSNotification.Name("AVPlayerItemDidPlayToEndTimeNotification"), object: videoPlayer?.currentItem)
    }
    
    @objc func videoDidPlayToEnd(_ notification: Notification) {
        continueBtn.isHidden = false
        UIView.animate(withDuration: 0.5) {
            self.continueBtn.alpha = 1
        }
    }
    
    @IBAction func continueTapped(_ sender: Any) {
        performSegue(withIdentifier: "endActivity", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! AlertViewController
        UIView.animate(withDuration: 0.5, delay: 1, options: .curveEaseIn, animations: {
        }) { (success:Bool) in
            vc.endLogo = "mount_logo"
            vc.nextLogo = "gaia_logo"
            vc.nameLabel.text = "The Golden Mount".uppercased()
            vc.attractionLabel.text = "The Gaia".uppercased()
            vc.endAlertIn()
        }
        
    }
    

}
