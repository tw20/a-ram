//
//  AttractionInfoViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 1/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase

class AttractionInfoViewController: UIViewController {
    
    @IBOutlet weak var activityNameLabel: UILabel!
    @IBOutlet weak var attractionNameLabel: UILabel!
    @IBOutlet weak var activityLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var attractionName = ""
    var attractionInfo = ""
    var logoImage: Int!
    var activityCount = 0
    var db: Firestore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        db = Firestore.firestore()
        getInfo()
        setUpView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        scrollView.contentInset = UIEdgeInsets(top: (scrollView.frame.height)/3, left: 0, bottom: 0, right: 0)
        
        
    }
    
    func getInfo() {
        db.collection("attraction").whereField("name", isEqualTo: attractionName).getDocuments { (Document, error) in
            if let error = error {
                print("Error getting Documents: \(error)")
            }else {
                for document in Document!.documents {
                    let info = document.data()["information"] as! String
                    let activity = document.data()["activity"] as! Int
                    self.infoLabel.text = info
                    self.activityLabel.text = String(activity)
                }
            }
        }
    }
    
    func setUpView() {
        showSpriner(time: 1)
        attractionNameLabel.text = attractionName.uppercased()
        //infoLabel.text = attractionInfo
        //ctivityLabel.text = String(activityCount)
        logoImageView.image = UIImage(named: "/(logoImage)")
        infoView.layer.cornerRadius = 15
        
        let height = activityNameLabel.frame.size.height
        let pos = activityNameLabel.frame.origin.y
        let textSize = infoLabel.frame.size.height
        let sizeOfContent = height + textSize + pos + 40
        infoView.frame.size = CGSize(width: scrollView.frame.size.width, height: sizeOfContent)
        //scrollView.contentSize.height = sizeOfContent
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width, height: infoView.frame.size.height)

    }
    
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
