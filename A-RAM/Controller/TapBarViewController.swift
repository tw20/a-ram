//
//  TapBarViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 17/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class TapBarViewController: UITabBarController {

    private var handle: AuthStateDidChangeListenerHandle?
    let db = Firestore.firestore()
    let user = Auth.auth().currentUser
    var route: [String] = []
    var number: [Int] = []
    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vc = self.viewControllers![0] as! MapNavigationViewController //first view controller in the tabbar
        vc.route = defaults.object(forKey: Keys.route) as? [String] ?? [String]()
        vc.number = defaults.object(forKey: Keys.icon) as? [Int] ?? [Int]()
        self.selectedIndex = 0        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
        if Auth.auth().currentUser != nil {
        // User is signed in.
            self.checkRoute()
        } else {
            // No user is signed in.
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let controller = storyboard.instantiateViewController(identifier: "welcomeVC")
            self.present(controller, animated: false, completion: nil)
            }
        }
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
   
        Auth.auth().removeStateDidChangeListener(handle!)
    }

    
    func checkRoute() {
        if self.route.count == 0 && self.number.count == 0 {
            route = defaults.object(forKey: Keys.route) as? [String] ?? [String]()
            number = defaults.object(forKey: Keys.icon) as? [Int] ?? [Int]()
            
            if self.route.count == 0 && self.number.count == 0 {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(identifier: "SetRouteVC")
                self.present(controller, animated: false, completion: nil)
            }
            
        }
    }
//    func getRoute() {
//        route = defaults.object(forKey: Keys.route) as? [String] ?? [String]()
//        number = defaults.object(forKey: Keys.icon) as? [Int] ?? [Int]()
//    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
