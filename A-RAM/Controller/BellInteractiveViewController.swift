//
//  BellInteractiveViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 19/1/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import AVFoundation

class BellInteractiveViewController: UIViewController {

    @IBOutlet weak var bellBtn: UIButton!
    @IBOutlet weak var startlabel: UILabel!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var bellImageView: UIImageView!
    @IBOutlet weak var introView: UIView!
    
    var audio: AVAudioPlayer?
    var count = 0
    var bellImage: [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bellImage = createImageArray(total: 29, imagePrefix: "bell")
        introView.layer.cornerRadius = 15
        continueBtn.layer.cornerRadius = 25
        continueBtn.alpha = 0
        continueBtn.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func bellTapped(_ sender: Any) {
        bellChimed()
        if count == 3 {
            print("active")
            continueBtn.isHidden = false
            UIView.animate(withDuration: 0.5, delay: 2, options: .curveEaseOut, animations: {
                self.continueBtn.alpha = 1
            }, completion: nil)
//            UIView.animate(withDuration: 2.5) {
//                self.continueBtn.alpha = 1
//            }
        }
    }
    
    func bellChimed() {
        playSound()
        self.animate(imageView: bellImageView, images: bellImage)
        startlabel.isHidden = true
        UIView.animate(withDuration: 0.5) {
            self.bellBtn.alpha = 0
        }
        
        if count != 3 {
            self.reset(delay: 2)
            self.count += 1
            print(self.count)
        }
    }
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "bell_sound", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            audio = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /* iOS 10 and earlier require the following line:
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

            guard let audio = audio else { return }

            audio.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func createImageArray(total: Int, imagePrefix: String) -> [UIImage] {
        var imageArray: [UIImage] = []

        for imageCount in 0..<total {
            let imageName = "\(imagePrefix)-\(imageCount).png"
            let image = UIImage(named: imageName)!

            imageArray.append(image)
        }

        return imageArray
    }
    
    func animate(imageView: UIImageView, images: [UIImage]) {
        imageView.animationImages = images
        imageView.animationDuration = 1
        imageView.animationRepeatCount = 1
        imageView.startAnimating()
    }
    
    func reset(delay: Double) {
        let timer = Timer.scheduledTimer(withTimeInterval: delay, repeats: false) { (timer) in
            if self.count != 3 {
                UIView.animate(withDuration: 0.5) {
                self.bellBtn.alpha = 1
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                }
            }
        }
        RunLoop.current.add(timer, forMode: .common)
    }
    
    @IBAction func continueTapped(_ sender: Any) {
        performSegue(withIdentifier: "endActivity", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! AlertViewController
        UIView.animate(withDuration: 0.5, delay: 1, options: .curveEaseIn, animations: {
        }) { (success:Bool) in
            vc.nameLabel.text = "BELLS"
            vc.endLogo = "bell_logo"
            vc.nextLogo = "urn_logo"
            vc.endAlertIn()
        }
    }
    
        
}
