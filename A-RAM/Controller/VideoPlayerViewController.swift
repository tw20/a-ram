//
//  VideoPlayerViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 8/1/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class VideoPlayerViewController: UIViewController {

    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var videoView: UIView!
    
    var segueName = "Buddha's Relic"
    var segueInfo = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    var segueImage = "urn_logo"
    var videoURL: String = "https://firebasestorage.googleapis.com/v0/b/a-ram-bf6a6.appspot.com/o/attraction%2F14_Relic_Animation.mp4?alt=media&token=559a096b-45b2-40e9-af9f-b44ee36a5f58"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
        nextBtn.layer.cornerRadius = 25
        nextBtn.isHidden = true
        nextBtn.alpha = 0
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        

    }
    
//    override var shouldAutorotate: Bool {
//        return false
//    }
//
//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        return .landscapeRight
//    }
//
//    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
//        return .landscapeRight
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        setUpView()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait  , andRotateTo: UIInterfaceOrientation.portrait)
        // Don't forget to reset when view is being removed
        //AppUtility.lockOrientation(.all)
    }
    
    private func setUpView() {

        let path = URL(string: videoURL)!
        let player = AVPlayer(url: path)
        
        let newLayer = AVPlayerLayer(player: player)
        newLayer.frame = self.videoView.frame
        self.videoView.layer.addSublayer(newLayer)
        newLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
//        let controller = AVPlayerViewController()
//        controller.player = player
//        controller.view.frame = self.view.frame
//        self.view.addSubview(controller.view)
//        self.addChild(controller)
        player.play()
        //player.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        
//        NotificationCenter.default.addObserver(self, selector: Selector(("playerDidFinishPlaying:")), name: NSNotification.Name(rawValue: "AVPlayerItemDidPlayToEndTime"), object: player.currentItem)

    }
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        print("Video Finished")
        nextBtn.isHidden = false
        UIView.animate(withDuration: 0.5) {
            self.nextBtn.alpha = 1
        }
        
    }
        
    @IBAction func nextTapped(_ sender: Any) {
            self.performSegue(withIdentifier: "information", sender: self)
        }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var vc = segue.destination as! InformationViewController
        vc.attractionName = segueName
        vc.attractionInfo = segueInfo
        vc.imageName = segueImage
    }
    }

