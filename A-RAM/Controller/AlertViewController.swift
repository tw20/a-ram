//
//  TapBarViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 28/1/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class AlertViewController: UIViewController {

    //recordAlert//
    @IBOutlet weak var endBlurEffectView: UIVisualEffectView!
    @IBOutlet var endView: UIView!
    @IBOutlet weak var endHeaderView: UIView!
    @IBOutlet weak var completeView: UIView!
    @IBOutlet weak var obvUIImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var completeLabel: UILabel!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var nextUIImageView: UIImageView!
    @IBOutlet weak var nextLabel: UILabel!
    @IBOutlet weak var attractionLabel: UILabel!
    @IBOutlet weak var recordBtn: UIButton!
    var blurEffect: UIVisualEffect?
    var effect: UIVisualEffect?
    var endLogo = ""
    var nextLogo = ""
    //recordAlert//
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpEndAlert()
        // Do any additional setup after loading the view.
    }
    
    func endAnimation() {
        self.iconAnimation(image: obvUIImageView, animationOptions: .curveEaseIn, delay: 2.0)
        self.completeAnimation(label: nameLabel, animationOptions: .curveEaseOut, delay: 0.5)
        self.completeAnimation(label: completeLabel, animationOptions: .curveEaseOut, delay: 1.0)
        self.transition(view1: completeView, view2: nextView, animationOptions: .curveEaseIn, delay: 2.0)
        self.iconAnimation(image: nextUIImageView, animationOptions: .curveEaseIn, delay: 2.5)
        self.nextAnimation(label: nextLabel, animationOptions: .curveEaseOut, delay: 3.0)
        self.nextAnimation(label: attractionLabel, animationOptions: .curveEaseOut, delay: 3.5)
        self.btnAnimation(button: recordBtn, animationOptions: .curveEaseOut, delay: 4.0)
    }
    
    private func setUpEndAlert() {
        endHeaderView.roundCorners(cornerRadius: 15)
        recordBtn.layer.cornerRadius = 20
        completeView.layer.cornerRadius = 15
        nextView.layer.cornerRadius = 20
        endView.layer.cornerRadius = 20
        endView.isHidden = true
        completeView.alpha = 1
        nextView.alpha = 0
        nameLabel.alpha = 0
        completeLabel.alpha = 0
        nextLabel.alpha = 0
        attractionLabel.alpha = 0
        blurEffect = endBlurEffectView.effect
        endBlurEffectView.isHidden = true
        endBlurEffectView.effect = nil
        endBlurEffectView.alpha = 0
    }
    
    private func setUpEndHeader() {
        let path = URL(fileURLWithPath: Bundle.main.path(forResource: "Record_Header", ofType: "mp4")!)
        let player = AVPlayer(url: path)

        let newPlayer = AVPlayerLayer(player: player)
        newPlayer.frame = CGRect(x: 0, y: 0, width: self.endHeaderView.frame.size.width, height: self.endHeaderView.frame.size.height)
        self.endHeaderView.layer.addSublayer(newPlayer)
        newPlayer.videoGravity = AVLayerVideoGravity.resizeAspectFill

        player.play()
        player.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none

        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.videoDidPlayToEnd(_:)), name: NSNotification.Name("AVPlayerItemDidPlayToEndTimeNotification"), object: player.currentItem)
    }
    
    @objc func videoDidPlayToEnd(_ notification: Notification) {
        let player: AVPlayerItem = notification.object as! AVPlayerItem
        player.seek(to: CMTime.zero)
    }
    
    func endAlertIn() {
        setUpEndHeader()
        obvUIImageView.image = UIImage(named: endLogo)
        nextUIImageView.image = UIImage(named: nextLogo)
        endAnimation()
        self.view.addSubview(endBlurEffectView)
        endView.center = self.view.center
        endView.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
        endView.alpha = 0
        
        UIView.animate(withDuration: 0.5) {
            self.endBlurEffectView.isHidden = false
            self.endBlurEffectView.effect = self.effect
            self.endBlurEffectView.alpha = 1
            self.endView.isHidden = false
            self.endView.alpha = 1
            self.endView.transform = CGAffineTransform.identity
        }
        
    }
    
    func endAlertOut() {
                UIView.animate(withDuration: 0.5, animations: {
                    self.endBlurEffectView.alpha = 0
                    self.endView.alpha = 0
                    self.endView.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
                    self.endBlurEffectView.effect = nil
                }) { (success:Bool) in
                    self.endView.isHidden = true
                    self.endBlurEffectView.isHidden = true
                    self.endBlurEffectView.removeFromSuperview()
                    self.navigationController?.pushViewController(ViewController(), animated: true)
                }
    //        db.collection("route").document("TEST").updateData(["isArcheived" : true]) { (error) in
    //            if error != nil {
    //                print(error)
    //            }else {
    //                print("ACTIVITY IS ARCHEIVED!")
    //            }
    //        }
        }
    
    
    @IBAction func endTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.endAlertOut()
        }) { (Success:Bool) in
            self.performSegue(withIdentifier: "backToHome", sender: nil)
        }
    }
    
    


}

extension UIViewController {
    func iconAnimation(image: UIImageView, animationOptions: UIImageView.AnimationOptions, delay: Double) {
        let timeDelay: TimeInterval = delay
        UIImageView.animate(withDuration: 0.5, delay: timeDelay, options: animationOptions, animations: {
          image.transform = CGAffineTransform(translationX: -15,y: 0)
          image.alpha = 1
        }, completion: nil)
    }
    
    func completeAnimation(label: UILabel, animationOptions: UILabel.AnimationOptions, delay: Double) {
      //let defaultYMovement: CGFloat = 240
        let timeDelay: TimeInterval = delay
        UILabel.animate(withDuration: 1, delay: timeDelay, options: animationOptions, animations: {
        label.transform = CGAffineTransform(translationX: 0,y: -15)
        label.alpha = 1
      }, completion: nil)
    }
    
    func transition(view1: UIView,view2: UIView, animationOptions: UILabel.AnimationOptions, delay: Double) {
      //let defaultYMovement: CGFloat = 240
        let timeDelay: TimeInterval = delay
        UIView.animate(withDuration: 0.5, delay: timeDelay, options: .curveEaseIn, animations: {
            view1.alpha = 0
            view2.alpha = 1
        }, completion: nil)
    }
    
    func nextAnimation(label: UILabel, animationOptions: UILabel.AnimationOptions, delay: Double) {
      //let defaultYMovement: CGFloat = 240
        let timeDelay: TimeInterval = delay
        UILabel.animate(withDuration: 1, delay: timeDelay, options: animationOptions, animations: {
        label.transform = CGAffineTransform(translationX: 0,y: -15)
        label.alpha = 1
      }, completion: nil)
    }
    
    func btnAnimation(button: UIButton, animationOptions: UIButton.AnimationOptions, delay: Double) {
        let timeDelay: TimeInterval = delay
        UIButton.animate(withDuration: 0.5, delay: timeDelay, options: animationOptions, animations: {
          button.transform = CGAffineTransform(translationX: 0,y: -15)
          button.alpha = 1
        }, completion: nil)
    }
}

extension UIView {
    func roundCorners(cornerRadius: Double) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
}

