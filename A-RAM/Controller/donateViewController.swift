//
//  donateViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 2/12/2562 BE.
//  Copyright © 2562 Pakkapong Somboon. All rights reserved.
//

import UIKit
import AVKit
import Firebase

class donateViewController: UIViewController {

    
    
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var badPredictionView: UIView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var donateBtn: UIButton!
    @IBOutlet weak var donateInfoView: UIView!
    @IBOutlet weak var confirmBtn: UIButton!
    
    var videoPlayer: AVPlayer?
    var videoPlayerLayer: AVPlayerLayer?
    var effect: UIVisualEffect?
    
    var db: Firestore!

    var cancelImage:UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        db = Firestore.firestore()
        confirmBtn.layer.cornerRadius = 25
        
        effect = visualEffectView.effect
        visualEffectView.effect = nil
        
        visualEffectView.isHidden = true
        
        badPredictionView.layer.cornerRadius = 15
        continueBtn.layer.cornerRadius = 25
        donateBtn.layer.cornerRadius = 25
        donateBtn.layer.borderWidth = 2
        donateBtn.layer.borderColor = UIColor(red:0.47, green:0.54, blue:0.51, alpha:1.0).cgColor
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        infoIn()
    }
    
    @IBAction func confirmTapped(_ sender: Any) {
        performSegue(withIdentifier: "endActivity", sender: self)
//        self.setUpView()
//        self.animateIn()
//        self.endAnimation()
    }
    
    private func setUpVideo() {
        let path = URL(fileURLWithPath: Bundle.main.path(forResource: "Esiimsi_Donate", ofType: "mp4")!)
        let player = AVPlayer(url: path)

        let newPlayer = AVPlayerLayer(player: player)
        newPlayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.layer.insertSublayer(newPlayer, at: 0)
        newPlayer.videoGravity = AVLayerVideoGravity.resizeAspectFill

        player.play()
        player.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        NotificationCenter.default.addObserver(self, selector: #selector(donateViewController.videoDidPlayToEnd(_:)), name: NSNotification.Name("AVPlayerItemDidPlayToEndTimeNotification"), object: player.currentItem)

    }

    @objc func videoDidPlayToEnd(_ notification: Notification) {
        let player: AVPlayerItem = notification.object as! AVPlayerItem
        player.seek(to: CMTime.zero, completionHandler: nil)
    }
    
    @IBAction func isEndTapped(_ sender: Any) {
        //archeiveUpdate
        db.collection("route").document("TEST").updateData(["isArcheived" : true]) { (error) in
            if error != nil {
                print(error)
            }else {
                print("ACTIVITY IS ARCHEIVED!")
            }
        }
        
    }
    
    func infoIn() {
        view.addSubview(badPredictionView)
        badPredictionView.center = self.view.center
        badPredictionView.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
        badPredictionView.isHidden = false
        badPredictionView.alpha = 0
        
        UIView.animate(withDuration: 0.5) {
            self.visualEffectView.isHidden = false
            self.visualEffectView.effect = self.effect
            self.visualEffectView.alpha = 1
            self.badPredictionView.alpha = 1
            self.badPredictionView.transform = CGAffineTransform.identity
        }
    }
    
    func infoOut() {
//        UIView.animate(withDuration: 0.5) {
//            self.visualEffectView.effect = nil
//            self.badPredictionView.alpha = 0
//            self.badPredictionView.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
//        }
//        UIView.animate(withDuration: 1, delay: 1.5, options: .curveEaseOut, animations: {
//            self.badPredictionView.isHidden = true
//            self.visualEffectView.isHidden = true
//        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.visualEffectView.alpha = 0
            self.badPredictionView.alpha = 0
            self.badPredictionView.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
            self.visualEffectView.effect = nil
        }) { (success:Bool) in
            self.badPredictionView.isHidden = true
            self.visualEffectView.isHidden = true
            self.visualEffectView.removeFromSuperview()
        }
        
    }
    
    @IBAction func continueBtnTapped(_ sender: Any) {
        infoOut()
        performSegue(withIdentifier: "endActivity", sender: self)
//        animateIn()
//        setUpView()
//        endAnimation()
    }
    
    @IBAction func donateTapped(_ sender: Any) {
        infoOut()
        setUpVideo()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var vc = segue.destination as! AlertViewController
        UIView.animate(withDuration: 0.5, delay: 1, options: .curveEaseIn, animations: {
        }) { (success:Bool) in
            vc.nameLabel.text = "ESIIMSI"
            vc.endLogo = "esiimsi_logo"
            vc.nextLogo = "buddha_logo"
            vc.attractionLabel.text = "BUDDHA IMAGE HALL"
            vc.endAlertIn()
        }
    }
    


}


