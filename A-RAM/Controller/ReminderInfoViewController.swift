//
//  ReminderInfoViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 4/3/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase

class ReminderInfoViewController: UIViewController {

    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var infoView: UIView!
    
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var startMonthLabel: UILabel!
    @IBOutlet weak var startDayLabel: UILabel!
    @IBOutlet weak var eventInfoTextView: UITextView!
    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var index: Int = 0
    var id: String = ""
    var eventName: String = ""
    var eventInfo:String = ""
    var start: Date?
    var end: Date?
    let db = Firestore.firestore()
    let dateFormatter = DateFormatter()
    let user = Auth.auth().currentUser!
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
        getInfo()
        // Do any additional setup after loading the view.
    }
    

    func setUpView() {
        showSpriner(time: 1)
        infoView.roundCorners(cornerRadius: 15)
        deleteBtn.layer.cornerRadius = 25
        deleteBtn.layer.borderWidth = 2
        deleteBtn.layer.borderColor = UIColor(red:0.47, green:0.54, blue:0.51, alpha:1.0).cgColor
        eventNameLabel.text = eventName
        addBackNavSwipe()
    }
    
    func getInfo() {
        db.collection("user").document(user.uid).collection("reminder").whereField("name", isEqualTo: eventName).getDocuments { (snapshot, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }else {
                for document in snapshot!.documents {
                    self.id = document.documentID as! String
                    self.eventInfo = document.data()["information"] as! String
                    self.start = (document.data()["start"] as! Timestamp).dateValue()
                    self.end = (document.data()["end"] as! Timestamp).dateValue()
                    
                    self.eventInfoTextView.text = self.eventInfo
                    // convert time
                    let dayFormatter = self.dateFormatter
                    dayFormatter.dateFormat = "d"
                    dayFormatter.timeZone = TimeZone(abbreviation: "GMT+7")
                    dayFormatter.locale = NSLocale.current
                    self.startDayLabel.text = dayFormatter.string(from: self.start!)
                    let monthFormatter = self.dateFormatter
                    monthFormatter.dateFormat = "MMMM"
                    monthFormatter.timeZone = TimeZone(abbreviation: "GMT+7")
                    monthFormatter.locale = NSLocale.current
                    self.startMonthLabel.text = monthFormatter.string(from: self.start!)
                    let timeFormatter = self.dateFormatter
                    timeFormatter.dateFormat = "h:mma"
                    timeFormatter.timeZone = TimeZone(abbreviation: "GMT+7")
                    timeFormatter.locale = NSLocale.current
                    self.startTimeLabel.text = timeFormatter.string(from: self.start!)
                    self.endTimeLabel.text = timeFormatter.string(from: self.end!)
                    
                }
            }
        }
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        db.collection("user").document(user.uid).collection("reminder").document(id).delete { (error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }else {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let desVC = mainStoryboard.instantiateViewController(identifier: "reminderVC") as! ReminderViewController
                desVC.eventName.remove(at: self.index)
                desVC.eventStart.remove(at: self.index)
                //desVC.logoImage = attractionImage[indexPath.item]
                
                self.navigationController?.popToViewController(desVC, animated: true)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
