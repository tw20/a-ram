//
//  WelcomeViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 24/1/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase
import FacebookCore
import FacebookLogin
import FBSDKCoreKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var emailLoginBtn: UIButton!
    @IBOutlet weak var googleLoginBtn: UIButton!
    @IBOutlet weak var facebookLoginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElement()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let frame = self.navigationController?.navigationBar.frame

        
        let height: CGFloat = 200
        let bounds = self.navigationController!.navigationBar.bounds
        self.navigationController?.navigationBar.frame = CGRect(x: (frame?.origin.x)!, y: (frame?.origin.y)!, width: bounds.width, height: bounds.height + height)
    }
    
    func setUpElement() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        emailLoginBtn.layer.cornerRadius = 25
        googleLoginBtn.layer.cornerRadius = 25
        facebookLoginBtn.layer.cornerRadius = 25
        
//        facebookLoginBtn.addTarget(self, action: #selector(handleSignInWithFacebookTapped), for: .touchUpInside)
    }
    
//    @objc func handleSignInWithFacebookTapped() {
//        let loginManager = LoginManager()
//        loginManager.logIn(permissions: [.publicProfile, .email, ], viewController: self) { (result) in
//            switch result {
//            case .success(granted: let grantedPermission, declined: let declinePermission, token: let accessToken):
//                self.signIntoFirebase()
//            case .failed(let error):
//                print(error)
//            case .cancelled:
//                print("Cancelled")
//            }
//        }
//    }
    
//    fileprivate func signIntoFirebase() {
//        let accessToken = AccessToken.current
//        guard let accessTokenString = accessToken?.tokenString else { return }
//        let credential = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
//        Auth.auth().signIn(with: credential) { (user, error) in
//            if let error = error {
//                print(error)
//                return
//            }
//            print("Successfully authenticated with Friebase.")
//            self.dismiss(animated: true, completion: nil)
//            }
//        }
    
    @IBAction func registerTapped(_ sender: Any) {
        performSegue(withIdentifier: "registerSegue", sender: self)
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        performSegue(withIdentifier: "loginSegue", sender: self)
    }
    
}
