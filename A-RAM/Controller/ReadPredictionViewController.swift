//
//  readPredictionViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 25/11/2562 BE.
//  Copyright © 2562 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase


class readPredictionViewController: UIViewController {

    @IBOutlet weak var predictionTextView: UITextView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var endView: UIView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var readView: UIView!
    @IBOutlet weak var predictionNumber: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    
    var effect: UIVisualEffect?
    var db: Firestore!
    var number: Int = 0
    var esiimsiPrediction: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        db = Firestore.firestore()

        effect = visualEffectView.effect
        visualEffectView.effect = nil
        endView.isHidden = true
        visualEffectView.isHidden = true
        continueBtn.layer.cornerRadius = 20
        endView.layer.cornerRadius = 25
        saveBtn.layer.cornerRadius = 25
        skipBtn.layer.cornerRadius = 25
        skipBtn.layer.borderWidth = 2
        skipBtn.layer.borderColor = UIColor(red:0.90, green:0.75, blue:0.51, alpha:1.0).cgColor
        readView.layer.cornerRadius = 25
        getPrediction()
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        predictionNumber.text = "NO.\(number)"
        predictionTextView.text = "\(esiimsiPrediction)"
        predictionAnimation(view: self.readView, animationOptions: .curveEaseOut)
    }
    @IBAction func saveDidTapped(_ sender: Any) {
        db.collection("esiimsi-save").addDocument(data: ["number": number, "prediction": esiimsiPrediction, "date": Date()])
        self.animateIn()
    }
    
    func getPrediction() {
        //show loading
        db.collection("esiimsi").whereField("number", isEqualTo: number).getDocuments { (snapshot, error) in
            //stop loading
            if error != nil {
                print(error!)
            }else{
                for document in (snapshot!.documents) {
                    print(document.data())
                    let basePrediction = document.data()["prediction"] as! String
                    self.esiimsiPrediction = basePrediction
                    self.predictionTextView.text = basePrediction
                    //print(self.prediction)
                }
            }

        }
        
    }
    
    func predictionAnimation(view: UIView, animationOptions: UIView.AnimationOptions) {
      //let defaultYMovement: CGFloat = 240
        UIView.animate(withDuration: 0.5, delay: 0, options: animationOptions, animations: {
        view.transform = CGAffineTransform(translationX: 0,y: -560)
      }, completion: nil)
    }
    
    @IBAction func isEndTapped(_ sender: Any) {
        db.collection("route").document("TEST").updateData(["isArcheived" : true]) { (error) in
            if error != nil {
                print(error)
            }else {
                print("ACTIVITY IS ARCHEIVED!")
            }
        }
    }
    
    func animateIn() {
        endView.center = self.view.center
        endView.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
        endView.alpha = 0
        
        UIView.animate(withDuration: 0.5) {
            self.endView.isHidden = false
            self.visualEffectView.isHidden = false
            self.visualEffectView.effect = self.effect
            self.endView.alpha = 1
            self.endView.transform = CGAffineTransform.identity
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
