//
//  EsiimsiViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 17/11/2562 BE.
//  Copyright © 2562 Pakkapong Somboon. All rights reserved.
//

import UIKit
import CoreMotion
import AVFoundation
import AVKit
import AudioToolbox
import Firebase

class EsiimsiViewController: UIViewController {
    
    @IBOutlet weak var tutorialContentView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet var tutorialView: UIView!
    @IBOutlet weak var readView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var predictionTextView: UITextView!
    @IBOutlet weak var predictionNumber: UILabel!
    
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var introView: UIView!
    @IBOutlet weak var esiimsiNumView: UIView!
    @IBOutlet weak var esiimsiNumber: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var esiimsiImageView: UIImageView!
    //@IBOutlet weak var esiimsiView: UIView!
    
    var motionManager = CMMotionManager()
    var esiimsiImage: [UIImage] = []
    var count:Int = 0
    var number: Int = 0
    var status = ""
    var check = false
    
    var db: Firestore!
    var esiimsiPrediction: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        esiimsiImage = createImageArray(total: 29, imagePrefix: "Esiimsi")
        db = Firestore.firestore()
        introView.layer.cornerRadius = 15
        startLabel.isHidden = false
        
        
        //**----------Prediction read-----------**//
        nextBtn.layer.cornerRadius = 25
        saveBtn.layer.cornerRadius = 25
        saveBtn.layer.borderWidth = 2
        saveBtn.layer.borderColor = UIColor(red:0.47, green:0.54, blue:0.51, alpha:1.0).cgColor
        readView.layer.cornerRadius = 25
        
        //**----------Prediction read-----------**//
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setUpLoading()
        UIView.animate(withDuration: 1, animations: {
            self.tutorialView.alpha = 1
            self.tutorialContentView.alpha = 1
        })
        
        motionManager.gyroUpdateInterval = 0.2
        motionManager.startGyroUpdates(to: OperationQueue.current!) { (data, error) in
            if let myData = data {
                //print(myData.rotationRate.z)
                if myData.rotationRate.x < -3
                {
                    //print("YOU TILTED YOUR DEVICE")
                    self.startLabel.isHidden = true
                    //self.esiimsiShake()
                    self.animate(imageView: self.esiimsiImageView, images: self.esiimsiImage)
                    
                    if self.check == false {
                        self.count += 1
                        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                        self.randomNumber()
                        self.getPrediction()
                        self.predictionNumber.text = "NO.\(self.number)"
                        self.predictionTextView.text = "\(self.esiimsiPrediction)"
                    }
                    
                }
                if self.count >= 3 {
                    
                    AudioServicesPlaySystemSound(SystemSoundID(1000))
                    self.esiimsiAnimation(view: self.esiimsiNumView, animationOptions: .curveEaseIn)
                    self.esiimsiHidden(view: self.esiimsiImageView, animationOptions: .curveEaseIn)
                    self.predictionAnimation(view: self.readView, animationOptions: .curveEaseOut)
                    self.count = 0
                    self.check = true
                }
            }
            
        }
        
    }
    
    
    
    func setUpLoading() {
        tutorialView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        tutorialView.backgroundColor = .white
        tutorialContentView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        setUpVideo()
        closeBtn.layer.cornerRadius = 25
        self.view.addSubview(tutorialView)
        self.tutorialView.addSubview(tutorialContentView)
    }

    private func setUpVideo() {
        let path = URL(fileURLWithPath: Bundle.main.path(forResource: "tutorial_esiimsi", ofType: "mp4")!)
        let player = AVPlayer(url: path)

        let newPlayer = AVPlayerLayer(player: player)
        newPlayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.tutorialView.layer.addSublayer(newPlayer)
        newPlayer.videoGravity = AVLayerVideoGravity.resizeAspectFill

        player.play()
        player.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        NotificationCenter.default.addObserver(self, selector: #selector(EsiimsiViewController.videoDidPlayToEnd(_:)), name: NSNotification.Name("AVPlayerItemDidPlayToEndTimeNotification"), object: player.currentItem)

    }

    @objc func videoDidPlayToEnd(_ notification: Notification) {
        let player: AVPlayerItem = notification.object as! AVPlayerItem
        player.seek(to: CMTime.zero, completionHandler: nil)
    }
    
    @IBAction func saveDidTapped(_ sender: Any) {
        self.showToast(message: "YOUR PREDICTION HAS BEEN SAVED!")
        db.collection("esiimsi-save").addDocument(data: ["number": number, "prediction": esiimsiPrediction, "date": Date()])
    }
    
    func getPrediction() {
        //show loading
        db.collection("attraction").document("6").collection("esiimsi").whereField("number", isEqualTo: number).getDocuments { (snapshot, error) in
            //stop loading
            if error != nil {
                print(error!)
            }else{
                for document in (snapshot!.documents) {
                    //print(document.data())
                    let basePrediction = document.data()["prediction"] as! String
                    let status = document.data()["status"] as! String
                    self.esiimsiPrediction = basePrediction
                    self.predictionTextView.text = basePrediction
                    if status == "good" {
//                        statusImageView.image = UIImage(named: "esiimsi-good")
//                        statusLabel.text = "It's good, Click save".uppercased()
                    } else if status == "not good" {
//                        statusImageView.image = UIImage(named: "esiimsi-notgood")
//                        statusLabel.text = "It's not good, Clikc next to donate".uppercased()
                    }
                    //print(self.prediction)
                }
            }

        }
        
    }
    
    
    func createImageArray(total: Int, imagePrefix: String) -> [UIImage] {
        var imageArray: [UIImage] = []

        for imageCount in 0..<total {
            let imageName = "\(imagePrefix)-\(imageCount+1).png"
            let image = UIImage(named: imageName)!

            imageArray.append(image)
        }

        return imageArray
    }
    
    func animate(imageView: UIImageView, images: [UIImage]) {
        imageView.animationImages = images
        imageView.animationDuration = 1
        imageView.animationRepeatCount = 1
        imageView.startAnimating()
    }
    
    func randomNumber() {
        let ranNum = Int.random(in: 1...15)
        number = Int(ranNum)        
        numberLabel.text = String(number)
    }
    
    
    
    
    func esiimsiAnimation(view: UIView, animationOptions: UIView.AnimationOptions) {
        UIView.animate(withDuration: 1, delay: 1, options: animationOptions, animations: {
        view.transform = CGAffineTransform(translationX: 0,y: -300)
      }, completion: nil)
    }
        
    func esiimsiHidden(view: UIView, animationOptions: UIView.AnimationOptions)
    {
        UIView.animate(withDuration: 0.5, delay: 1, options: animationOptions, animations: {
        view.alpha = 0
      }, completion: nil)
    }
    
    func predictionAnimation(view: UIView, animationOptions: UIView.AnimationOptions) {
        UIView.animate(withDuration: 1, delay: 3, options: animationOptions, animations: {
        view.transform = CGAffineTransform(translationX: 0,y: -560)
            self.introView.alpha = 0
      }, completion: nil)
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.tutorialView.alpha = 0
        }) { (success:Bool) in
            self.tutorialView.removeFromSuperview()
        }
        
        
    }
    
    
    @IBAction func nextTapped(_ sender: Any) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desVC = mainStoryboard.instantiateViewController(identifier: "Donation") as! donateViewController
        self.present(desVC, animated: true, completion: nil
        )
    }
    
}

extension EsiimsiViewController {
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: 40, y: 40, width: 334, height: 60))
        toastLabel.textAlignment = .center
        toastLabel.backgroundColor = UIColor(red:0.73, green:0.35, blue:0.20, alpha:1.0)
        toastLabel.textColor = UIColor.white
        toastLabel.alpha = 1.0
        toastLabel.text = message
        toastLabel.font = UIFont(name: "K2D-Bold", size: 16)
        toastLabel.layer.cornerRadius = 15
        toastLabel.clipsToBounds = true
        self.view.addSubview(toastLabel)
        
        UIView.animate(withDuration: 3.0, delay: 1.0, options: .curveEaseInOut, animations: {
            toastLabel.alpha = 0
        }) { (isCompleted) in
            toastLabel.removeFromSuperview()
        }
    }
}
