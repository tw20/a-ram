//
//  ReminderViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 21/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher
import AVKit
import AVFoundation

class ReminderViewController: UIViewController {
    
    @IBOutlet weak var logoView: UIView!
    @IBOutlet var loadingView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let db = Firestore.firestore()
    let user = Auth.auth().currentUser!
    let rightNow = Date()
    let dateFormatter = DateFormatter()
    
    var eventName: [String] = []
    var eventStart:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.dataSource = self
        collectionView?.delegate = self
        getInfo()
        setUpView()
        // Do any additional setup after loading the view.
    }
    
    func setUpView() {
        showSpriner(time: 1)
        headerView.bottomRoundCorners(cornerRadius: 15)
        addBackNavSwipe()
    }
    
    func getInfo() {
        db.collection("user").document(user.uid).collection("reminder").getDocuments { (snapshot, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }else {
                for document in snapshot!.documents {
                    let fbName = document.data()["name"] as! String
                    let fbStart = (document.data()["start"] as! Timestamp).dateValue()
                    
                    let eventStartFormatter = self.dateFormatter
                    eventStartFormatter.dateFormat = "dd MMM"
                    let getStart = eventStartFormatter.string(from: fbStart)
//                    if fbStart < self.rightNow {
                        self.eventName.append(fbName)
                        self.eventStart.append(getStart)
                    //}
                }
            }
            self.collectionView.reloadData()
            self.loadingView.removeFromSuperview()
        }
    }
    
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ReminderViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if eventName.count > 0 {
            return eventName.count
        } else {
            return 1
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reminderCell", for: indexPath) as! ReminderCollectionViewCell
        cell.layer.cornerRadius = 15
        cell.dayView.layer.cornerRadius = 15
        if eventName.count != 0
        {
            cell.eventnameLabel.isHidden = false
            cell.dayView.isHidden = false
            cell.eventnameLabel.text = eventName[indexPath.item]
            cell.dayLabel.text = eventStart[indexPath.item]
            cell.bgImageView.image = UIImage(named: "route_bg")
        } else {
            cell.eventnameLabel.isHidden = true
            cell.dayView.isHidden = true
            cell.backgroundColor = .clear
            //cell.frame = CGRect(x: 0, y: 0, width: cell.contentView.frame.width, height: cell.contentView.frame.height)
            //cell.bgImageView.image = UIImage(named: "reminder_nil")
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if eventName.count > 0 {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desVC = mainStoryboard.instantiateViewController(identifier: "reminderInfoVC") as! ReminderInfoViewController
            desVC.eventName = eventName[indexPath.item]
            desVC.index = indexPath.item
            //desVC.logoImage = attractionImage[indexPath.item]
            
            self.navigationController?.pushViewController(desVC, animated: true)
        }
        
    }


}
