//
//  ZodiacInformationViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 19/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase

class ZodiacInformationViewController: UIViewController {

    @IBOutlet weak var zodiacLabel: UILabel!
    @IBOutlet weak var zodiacPronounceLabel: UILabel!
    @IBOutlet weak var zodiacInfoTextView: UITextView!
    @IBOutlet weak var zodiacImageView: UIImageView!
    @IBOutlet weak var infoView: UIView!
    
    let db = Firestore.firestore()
    let defaults = UserDefaults.standard
    var zodiac: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
        // Do any additional setup after loading the view.
    }
    
    func setUpView() {
        
        addBackNavSwipe()
        infoView.layer.cornerRadius = 15
        zodiac = defaults.string(forKey: Keys.zodiac)!
        zodiacLabel.text = zodiac.uppercased()
        zodiacImageView.image = UIImage(named: zodiac)
        db.collection("zodiac").document(zodiac.lowercased()).getDocument { (document, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }else {
                let getInfo = document?.data()!["information"] as! String
                let getPronounce = document?.data()!["pronounce"] as! String
                self.zodiacInfoTextView.text = getInfo
                self.zodiacPronounceLabel.text = getPronounce.uppercased()
            }
        }
    }

    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
