//
//  InformationViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 15/1/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit

class InformationViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var attractionLogoImageView: UIImageView!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var attractionNameLabel: UILabel!
    @IBOutlet weak var attractionInfoLabel: UILabel!
    var attractionName = ""
    var attractionInfo = ""
    var imageName = "urn_logo"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height)
        //scrollView.autoresizingMask = .flexibleHeight
        //scrollView.contentInset = UIEdgeInsets(top: 200, left: 0, bottom: 0, right: 0)
        //let scrollPoint = CGPoint(x: 0, y: contentView.frame.origin.y+200)
        //scrollView.setContentOffset(scrollPoint, animated: false)
        contentView.layer.cornerRadius = 15
        attractionNameLabel.text = attractionName.uppercased()
        attractionInfoLabel.text = attractionInfo
        continueBtn.layer.cornerRadius = 25
        attractionLogoImageView.image = UIImage(named: imageName)!

        
       
    AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        

    }

    @IBAction func continueTapped(_ sender: Any) {
        if self.attractionName == "Bells" {
            performSegue(withIdentifier: "bellActivity", sender: self)
        }else {
            performSegue(withIdentifier: "endActivity", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch attractionName {
                case "Bells":
                    let bellVC = segue.destination as! BellInteractiveViewController
                    //navigationController?.pushViewController(BellInteractiveViewController(), animated: true)
        //            bellVC.segueImage = imageName
        //            bellVC.segueName = attractionName
        //            bellVC.segueImage = imageName
                    

                default:
                    let vc = segue.destination as! AlertViewController
                    UIView.animate(withDuration: 0.5, delay: 1, options: .curveEaseIn, animations: {
                    }) { (success:Bool) in
                        vc.nameLabel.text = self.attractionName.uppercased()
                        vc.endLogo = self.imageName
                        vc.nextLogo = "birth_logo"
                        vc.attractionLabel.text = "BIRTHDAY BUDDHA STATUE"
                        vc.endAlertIn()
                    }
                }
    }
}
