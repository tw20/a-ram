//
//  LoginViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 8/12/2562 BE.
//  Copyright © 2562 Pakkapong Somboon. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    let db = Firestore.firestore()
    var route: [String] = []
    var number: [Int] = []
    var timer = Timer()
    var time: Double = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //self.navigationBar.shadowImage = UIImage()
        setUpElement()
    }
    
    func setUpElement() {
        navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        //errorLabel.alpha = 0
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        loginBtn.layer.cornerRadius = 25
        addBackNavSwipe()
        addDissmissTap()
        
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        showSpriner(time: 4)
        let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error {
                self.handlerFireAuthError(error)
                debugPrint(error.localizedDescription)
                //self.errorLabel.text = error!.localizedDescription
                //self.errorLabel.alpha = 1
            }else {
                self.getRoute()
                self.getInfo()
            }
        }
        Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false) { (t) in
            self.loggedIn()
        }
        
    }
    
    func getInfo() {
        self.db.collection("user").document(Auth.auth().currentUser!.uid).getDocument { (document, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }else {
                let getEmail = document?.data()!["email"] as! String
                let getUsername = document?.data()!["username"] as! String
                let getBirth = (document?.data()!["birthdate"] as! Timestamp).dateValue()
                let getGender = document?.data()!["gender"] as! String
                let getZodiac = document?.data()!["zodiac"] as! String
                self.saveDataToUserDefaults(email: getEmail, username: getUsername, birthdate: getBirth, gender: getGender, zodiac: getZodiac)
            }
        }
    }
    
    func getRoute() {
        self.db.collection("user").document(Auth.auth().currentUser!.uid).collection("route").getDocuments { (snapshot, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }else {
                for document in snapshot!.documents
                {
                    self.route.append(document.data()["name"] as! String)
                    self.number.append(document.data()["number"] as! Int)
                }
                self.saveRouteToUserDefaults(route: self.route, icon: self.number)
            }
        }
    }
    
    func loggedIn() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "mainVC") as! TapBarViewController
        controller.route = route
        controller.number = number
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
