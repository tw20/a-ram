//
//  ViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 27/10/2562 BE.
//  Copyright © 2562 Pakkapong Somboon. All rights reserved.
//

import UIKit
import CoreLocation
import AVKit
import AVFoundation
import Firebase
import FirebaseAuth
import UserNotifications

class ViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var accuracyLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var mapUIImageView: UIImageView!

    @IBOutlet weak var startLogoImageView: UIImageView!
    @IBOutlet weak var startHeaderView: UIView!
    @IBOutlet var startBlurEffectView: UIVisualEffectView!
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var start: UIButton!
    @IBOutlet weak var laterBtn: UIButton!
    @IBAction func startBtn(_ sender: AnyObject) {
        startAlertOut()
    }
    @IBOutlet weak var distanceReading: UILabel!
    @IBOutlet weak var attractionNameLabel: UILabel!

    var locationManager: CLLocationManager?
    var beaconsToRange: [CLBeaconRegion] = []
    let defaults = UserDefaults.standard
    let uuid = "E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"
    var identifier: String = ""
//    var name = ["The Golden Mount",
//                "The Gaia","Bells",
//                "Buddha's Relic",
//                "Birthday Buddha Statue",
//                "Esiimsi",
//                "Buddha Image Hall",
//                "The Ordination Hall",
//                "Sri Maha Bodhi Tree"]
//    var attractionImage: [UIImage] = [
//        UIImage(named: "1")!,
//        UIImage(named: "2")!,
//        UIImage(named: "3")!,
//        UIImage(named: "4")!,
//        UIImage(named: "5")!,
//        UIImage(named: "6")!,
//        UIImage(named: "7")!,
//        UIImage(named: "8")!,
//        UIImage(named: "9")!
//    ]
    var name: [String] = []
    var attractionImage: [Int] = []
    
    //data for segue//
    var imageName: String = ""
    var attractionName: String = ""
    var info: String = ""
    var animationURL: String = ""
    //data for segue - End//
    
    var count: Bool = false
    
    var db = Firestore.firestore()
    var effect: UIVisualEffect?
    let notificationCenter = UNUserNotificationCenter.current()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        scrollView.contentSize = CGSize(width: mapUIImageView.frame.width, height: mapView.frame.height)
        setUpBeacons()
        setUpStartAlert()
    }

    private func setUpBeacons() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        name = defaults.object(forKey: Keys.route) as? [String] ?? [String]()
        attractionImage = defaults.object(forKey: Keys.icon) as? [Int] ?? [Int]()
    }
    
    private func setUpStartAlert() {
        notificationCenter.requestAuthorization(options: [.alert, .sound]) { (grant, error) in
            let content = UNMutableNotificationContent()
            content.title = "Route Notification"
            content.body = "You're ready to get new information from new attraction!"
            let uuidd = UUID().uuidString
            //let trigger = UNNotificationTrigger(coder: NSCoder)
            
            //let request = UNNotificationRequest(identifier: uuid, content: content, trigger: setUpStartAlert())
        }
        
        effect = startBlurEffectView.effect
        startBlurEffectView.effect = nil
        //visualEffectView.isHidden = true
        startView.layer.cornerRadius = 15
        start.layer.cornerRadius = 25
        startView.roundCorners(cornerRadius: 15)
        laterBtn.layer.cornerRadius = 25
        laterBtn.layer.borderWidth = 2
        laterBtn.layer.borderColor = UIColor(red:0.47, green:0.54, blue:0.51, alpha:1.0).cgColor
    }
    
    
    
    private func setUpStartHeader() {
        let path = URL(fileURLWithPath: Bundle.main.path(forResource: "Beacon_Header", ofType: "mp4")!)
        let player = AVPlayer(url: path)
        
        let newPlayer = AVPlayerLayer(player: player)
        newPlayer.frame = self.startHeaderView.frame
        self.startHeaderView.layer.addSublayer(newPlayer)
        newPlayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        player.play()
        player.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.videoDidPlayToEnd(_:)), name: NSNotification.Name("AVPlayerItemDidPlayToEndTimeNotification"), object: player.currentItem)
    }
    
    @objc func videoDidPlayToEnd(_ notification: Notification) {
        let player: AVPlayerItem = notification.object as! AVPlayerItem
        player.seek(to: CMTime.zero, completionHandler: nil)
    }
    
    
    
    
    
    
    
    
    
    
    //BEACONS RANGING - START//
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            if status == .authorizedWhenInUse {
                if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                    let proximityUUID = UUID(uuidString: uuid)
                    let uuid = CLBeaconIdentityConstraint(uuid: proximityUUID!, major: 47985)
                    let beaconID = "myBeacon"
                    
                    let region = CLBeaconRegion(beaconIdentityConstraint: uuid, identifier: beaconID)
                    if CLLocationManager.isRangingAvailable() {
                        locationManager?.startMonitoring(for: region)
                        locationManager?.startRangingBeacons(satisfying: uuid)

                        // Store the beacon so that ranging can be stopped on demand.
                        beaconsToRange.append(region)
                    }
                        
                }
            }
        }
        
//        func startScanning() {
//                    let uuid = UUID(uuidString: "E2C56DB5-DFFB-48D2-B060-D0F5A71096E0")!
//                    let beaconRegion =  CLBeaconRegion(proximityUUID: uuid, major: 47985, minor: CLBeaconMinorValue(minor), identifier: "myBeacon")
//
//
//                    locationManager?.startRangingBeacons(in: beaconRegion)
//        }
    
//    func locationManager(_ manager: CLLocationManager,
//                didEnterRegion region: CLRegion) {
//        if region is CLBeaconRegion {
//            // Start ranging only if the devices supports this service.
////            if CLLocationManager.isRangingAvailable() {
////                locationManager?.startRangingBeacons(in: region as! CLBeaconRegion)
////
////                // Store the beacon so that ranging can be stopped on demand.
////                beaconsToRange.append(region as! CLBeaconRegion)
////            }
//        }
//    }
    
    
    func locationManager(_ manager: CLLocationManager,
            didRangeBeacons beacons: [CLBeacon],
            in region: CLBeaconRegion) {
    if beacons.count > 0 {
        let sorted = beacons.sorted { (b1, b2) -> Bool in
            let filter = b1.accuracy - b2.accuracy
            if  filter >= 2.50 {
                return b1.accuracy < b2.accuracy
            }
            return b1.accuracy > b2.accuracy
        }
        
        let filtered = sorted.filter { (sorted) -> Bool in
            sorted.accuracy != -1
        }
        let nearestBeacon = filtered.first ?? nil
        
        
        let major = CLBeaconMajorValue(truncating: (nearestBeacon?.major ?? 00))
        let minor = CLBeaconMinorValue(truncating: (nearestBeacon?.minor ?? 00))
        if nearestBeacon != nil{
            let distance = ((nearestBeacon!.accuracy*100).rounded()/100)
            update(distance: distance, major: major, minor: minor)
        }
        }
    }

    func update(distance:  CLLocationAccuracy, major: CLBeaconMajorValue, minor: CLBeaconMinorValue) {
        print("\(minor)  \(String(distance))")
        accuracyLabel.text = "\(minor)  \(String(distance))"
        
        UIView.animate(withDuration: 0.5) {
            if distance <= 1.5 {
                    if self.count == false {
                        self.startAlertIn(major: major, minor: minor)
                    }
                }
//                switch distance {
//                case 2.0:
//                    self.distanceReading.text = "OUT OF RANGE"
//
//                case 1.5:
//                    self.distanceReading.text = "FAR"
//
//
//                case 1:
//                    self.distanceReading.text = "NEAR"
//
//
//                case 0.5:
//                    self.distanceReading.text = "RIGHT HERE"
//
//                    print("\(distance) \(minor)")
//
//                default:
//                    self.distanceReading.text = "OUT OF RANGE"
//                }
            }
        }

//        func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
//            if let beacon = beacons.first {
//                update(distance: beacon.proximity)
//            } else {
//                update(distance: .unknown)
//            }
//        }
    
    func setApp(minor: CLBeaconMinorValue) {
        
        switch minor {
        case 19:
            attractionName = "The Golden Mount"
            attractionNameLabel.text = attractionName.uppercased()
            imageName = "mount_logo"
            //animationURL = ""
            startLogoImageView.image = UIImage(named: imageName)!
            identifier = "Ticket"
        
        case 12:
            attractionName = "The Gaia"
            attractionNameLabel.text = attractionName.uppercased()
            imageName = "gaia_logo"
            //animationURL = ""
            startLogoImageView.image = UIImage(named: imageName)!
            identifier = "Animation"
            
        case 13:
            attractionName = "Bells"
            attractionNameLabel.text = attractionName.uppercased()
            imageName = "bell_logo"
            //animationURL = ""
            startLogoImageView.image = UIImage(named: imageName)!
            identifier = "Animation"
            
        case 14:
            attractionName = "Buddha's Relic"
            attractionNameLabel.text = attractionName.uppercased()
            imageName = "urn_logo"
            //animationURL = ""
            startLogoImageView.image = UIImage(named: imageName)!
            identifier = "Animation"
            
        case 15:
            attractionName = "Birthday Buddha Statue"
            attractionNameLabel.text = attractionName.uppercased()
            imageName = "birth_logo"
            startLogoImageView.image = UIImage(named: imageName)!
            //var daySegue = ""
            //var postureSegue = ""
            identifier = "Birthday"
            
        case 16:
            attractionName = "Esiimsi"
            attractionNameLabel.text = attractionName.uppercased()
            imageName = "esiimsi_logo"
            startLogoImageView.image = UIImage(named: imageName)!
            identifier = "Esiimsi"
            
        case 17:
            attractionName = "Buddha Image Hall"
            attractionNameLabel.text = attractionName.uppercased()
            imageName = "buddha_logo"
            animationURL = ""
            startLogoImageView.image = UIImage(named: imageName)!
            identifier = "Animation"
            
        case 18:
            attractionName = "The Ordination Hall"
            attractionNameLabel.text = attractionName.uppercased()
            imageName = "meditation_logo"
            startLogoImageView.image = UIImage(named: imageName)!
            identifier = "Meditation"
            
//        case 19:
//            attractionName = "Sri Maha Bodhi Tree"
//            attractionNameLabel.text = attractionName.uppercased()
//            imageName = "tree_logo"
//            startLogoImageView.image = UIImage(named: imageName)!
//            identifier = "AR"
            
        default:
            attractionNameLabel.text = ""
        }
        getInfoForSegue(minor: minor)
        
        
    }
    
    func getInfoForSegue(minor: CLBeaconMinorValue) {
        db.collection("attraction").whereField("minor", isEqualTo: minor).getDocuments { (snapshot, error) in
            if let error = error {
                print("Error getting Documents: \(error)")
            }else {
                for document in snapshot!.documents {
                    let name = document.data()["name"] as! String
                    let information = document.data()["information"] as! String
                    let url = document.data()["animation"] as! String
                    self.attractionName = name
                    self.info = information
                    self.animationURL = url
                }
            }
        }
    }
    
    
    
    
    //BEACON RANGING - END//
    
    //ANIMATION - START//
        
    func startAlertIn(major: CLBeaconMajorValue, minor: CLBeaconMinorValue) {
            setUpStartHeader()
            setApp(minor: minor)
            startView.isHidden = false
            startView.alpha = 0
            self.view.addSubview(startBlurEffectView)
            startBlurEffectView.center = self.view.center
            startView.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
            self.count = true
            

            
//            UIView.animate(withDuration: 0.5) {
//                self.startView.alpha = 1
//                self.visualEffectView.isHidden = false
//                self.visualEffectView.effect = self.effect
//                self.startView.transform = CGAffineTransform.identity
//
//            }
            
            UIView.animate(withDuration: 0.5, delay: 1, animations: {
                self.startView.alpha = 1
                self.startBlurEffectView.isHidden = false
                self.startBlurEffectView.alpha = 1
                self.startBlurEffectView.effect = self.effect
                self.startView.transform = CGAffineTransform.identity
            }, completion: nil)
        }
    
        func startAlertOut() {
            
            UIView.animate(withDuration: 0.5, animations: {
                self.startBlurEffectView.alpha = 0
                self.startView.alpha = 0
                self.startView.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
                self.startBlurEffectView.effect = nil
            }) { (success:Bool) in
                self.startView.isHidden = true
                self.startBlurEffectView.isHidden = true
                self.startBlurEffectView.removeFromSuperview()
            }
        }
    
        
        
        
    
    
    
    //ANIMATION - END//
    
    
    @IBAction func startTapped(_ sender: Any) {
        switch identifier {
            
        case "Esiimsi":
            performSegue(withIdentifier: "esiimsiActivity", sender: self)
//            let activityViewController = self.storyboard?.instantiateViewController(withIdentifier: "Esiimsi") as! EsiimsiViewController
//            self.present(activityViewController, animated: true)
            print("Esiimsi")
            
        case "Animation":
            performSegue(withIdentifier: "videoActivity", sender: self)
            print("Animation")
            
        case "Birthday":
            performSegue(withIdentifier: "birthdayActivity", sender: self)
            print("Birthday")
            
//        case "Bells":
//            performSegue(withIdentifier: "bellsActivity", sender: self)
//            print("Bells")
            
        case "Ticket":
        performSegue(withIdentifier: "ticketActivity", sender: self)
        print("Ticket")
            
        case "Meditation":
        performSegue(withIdentifier: "meditationActivity", sender: self)
        print("Meditation")
            
//        case "AR":
//        performSegue(withIdentifier: "arActivity", sender: self)
//        print("AR")
            
        default:
            print("start")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch identifier {
        case "Esiimsi":
            let esiimsiVC = segue.destination as! EsiimsiViewController
            
        case "Animation":
            let animationVC = segue.destination as! VideoPlayerViewController
            animationVC.segueImage = imageName
            animationVC.segueName = attractionName
            animationVC.segueImage = imageName
            animationVC.segueInfo = info
            animationVC.videoURL = animationURL
            
        case "Birthday":
            let birthdayVC = segue.destination as! BirthdayBuddhaViewController
            
        case "Ticket":
            let TicketVC = segue.destination as! TicketViewController
            
        case "Meditation":
        let meditationVC = segue.destination as! MeditationViewController
            
//        case "AR":
//            let arVC = segue.destination as! ARViewController
            
        default:
            print("")
        }
        
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        startAlertOut()
        reset(delay: 1)
    }
    
    func reset(delay: Double) {
        let timer = Timer.scheduledTimer(withTimeInterval: delay, repeats: false) { (timer) in
            self.count = false
        }
        RunLoop.current.add(timer, forMode: .common)
    }
}



//extension UIView {
//    func roundCorners(cornerRadius: Double) {
//        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
//        let maskLayer = CAShapeLayer()
//        maskLayer.frame = self.bounds
//        maskLayer.path = path.cgPath
//        self.layer.mask = maskLayer
//    }
//}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return name.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "attractionCell", for: indexPath) as! CollectionViewCell
        cell.attractionLabel.text = name[indexPath.item]
        cell.attractionLogoImageView.image = UIImage(named: "\(attractionImage[indexPath.item])")
            
            
        cell.layer.cornerRadius = 15
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let desVC = mainStoryboard.instantiateViewController(identifier: "attractionInfo") as! AttractionInfoViewController
        desVC.attractionName = name[indexPath.item]
        desVC.logoImage = attractionImage[indexPath.item]
        //self.navigationController?.pushViewController(desVC, animated: true)
        self.present(desVC, animated: true, completion: nil)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
//        return CGSize(width: <#T##CGFloat#>, height: <#T##CGFloat#>)
//    }
}
