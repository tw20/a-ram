//
//  ZodiacViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 17/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase
import AVKit
import AVFoundation

class ZodiacViewController: UIViewController {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var zodiacCharacterImageView: UIImageView!
    @IBOutlet weak var zodiacNameLabel: UILabel!
    let db = Firestore.firestore()
    let defaults = UserDefaults.standard
    var zodiac: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        zodiac = defaults.string(forKey: Keys.zodiac)!
        getInfo()
        setUpView()
        setUpVideo()
        
        // Do any additional setup after loading the view.
    }
    
    func setUpView() {
        showSpriner(time: 3)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        startBtn.layer.cornerRadius = 25
        zodiacNameLabel.text = zodiac.uppercased()
        zodiacCharacterImageView.image = UIImage(named: zodiac)
    }
    
    func getInfo() {
        let user = Auth.auth().currentUser ?? nil
        db.collection("users").whereField("uid", isEqualTo: user?.uid).getDocuments { (snapshot, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }else {
                for document in snapshot!.documents {
                    let getZodiac = document.data()["zodiac"] as! String
                    self.zodiac = getZodiac
                }
            }
        }
    }
    
    private func setUpVideo() {
        let path = URL(fileURLWithPath: Bundle.main.path(forResource: "Zodiac", ofType: "mp4")!)
        let player = AVPlayer(url: path)

        let newPlayer = AVPlayerLayer(player: player)
        newPlayer.frame = CGRect(x: 0, y: 0, width: self.bgView.frame.size.width, height: self.bgView.frame.size.height)
        self.bgView.layer.addSublayer(newPlayer)
        newPlayer.videoGravity = AVLayerVideoGravity.resizeAspectFill

        player.play()
        player.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none

        NotificationCenter.default.addObserver(self, selector: #selector(ZodiacViewController.videoDidPlayToEnd(_:)), name: NSNotification.Name("AVPlayerItemDidPlayToEndTimeNotification"), object: player.currentItem)
    }
    
    @objc func videoDidPlayToEnd(_ notification: Notification) {
        let player: AVPlayerItem = notification.object as! AVPlayerItem
        player.seek(to: CMTime.zero)
    }
    
    @IBAction func startTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "SetRouteVC")
        self.present(controller, animated: true, completion: nil)
    }
    

}
