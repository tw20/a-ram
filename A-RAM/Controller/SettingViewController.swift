//
//  SettingViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 21/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingViewController: UIViewController {

    @IBOutlet weak var predictionBtn: UIButton!
    @IBOutlet weak var reminderBtn: UIButton!
    @IBOutlet weak var routeBtn: UIButton!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var logOutBtn: UIButton!
    @IBOutlet weak var headerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    func setUpView() {
        headerView.bottomRoundCorners(cornerRadius: 15)
        profileBtn.layer.cornerRadius = 15
        routeBtn.layer.cornerRadius = 15
        reminderBtn.layer.cornerRadius = 15
        predictionBtn.layer.cornerRadius = 15
        logOutBtn.layer.cornerRadius = 25
        logOutBtn.layer.borderWidth = 2
        logOutBtn.layer.borderColor = UIColor(red:0.47, green:0.54, blue:0.51, alpha:1.0).cgColor
        addBackNavSwipe()
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func profileTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "profileSegue", sender: self)
    }
    
    @IBAction func logOutTapped(_ sender: Any) {
        guard let user = Auth.auth().currentUser else { return }
        if !user.isAnonymous {
            try! Auth.auth().signOut()
            deleteDataFromUserDefaults()
            deleteRouteFromUserDefaults()
        }
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "welcomeVC")
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func routeTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "SetRouteVC")
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func reminderTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "reminderSegue", sender: self)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
