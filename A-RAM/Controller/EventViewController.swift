//
//  EventViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 12/2/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher
import AVKit
import AVFoundation

class EventViewController: UIViewController {

    @IBOutlet weak var logoView: UIView!
    @IBOutlet var loadingView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let calendar = Calendar.current
    let rightNow = Date()
    var neekWeekfromNow = Date()
    let dateFormatter = DateFormatter()
    let db: Firestore! = Firestore.firestore()
    
    var month: String = ""
    var eventName: [String] = []
    var eventStart: [String] = []
    var timeStart: [String] = []
    var timeEnd: [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCalendar()
        collectionView?.dataSource = self
        collectionView?.delegate = self
        getInfo()
        setUpView()
        setUpLoading()
        

    }
    
    func setUpView() {
        headerView.bottomRoundCorners(cornerRadius: 15)
        dateLabel.text = month
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    
    func setUpCalendar() {
        let startDay = (calendar.dateInterval(of: .day, for: rightNow)?.start)!
        let monthInterval = calendar.dateInterval(of: .month , for: rightNow)
        //print(weekInterval)
        let timePeriod = calendar.dateComponents([.minute], from: monthInterval!.start, to: monthInterval!.end)
        let minuteInMonth = timePeriod.minute!
        neekWeekfromNow = calendar.date(byAdding: .minute, value: minuteInMonth, to: startDay)!
        //Set up Month
        let monthFormatter = dateFormatter
        monthFormatter.dateFormat = "MMMM yyyy"
        monthFormatter.locale = Locale(identifier: "en_US")
        month = monthFormatter.string(from: rightNow)
    }
    
    func getInfo() {
        db.collection("event").getDocuments { (snapshot, error) in
            if let error = error {
                print("Error getting Doucuments: \(error)")
            }else {
                for document in snapshot!.documents {
                    let fbName = document.data()["name"] as! String
                    let fbStart = document.data()["start"] as! Timestamp
                    let fbEnd = document.data()["end"] as! Timestamp

                    let getStart = fbStart.dateValue()
                    let getEnd = fbEnd.dateValue()

                    let startTimeFormatter = self.dateFormatter
                    startTimeFormatter.dateFormat = "h:mma"
                    let timeStart = startTimeFormatter.string(from: getStart)

                    let eventStartFormatter = self.dateFormatter
                    eventStartFormatter.dateFormat = "dd MMM"
                    let Start = eventStartFormatter.string(from: getStart)

                    let endTimeFormatter = self.dateFormatter
                    endTimeFormatter.dateFormat = "h:mma"
                    let timeEnd = endTimeFormatter.string(from: getEnd)

                    if getStart >= self.rightNow && getStart <= self.neekWeekfromNow {
                        self.eventName.append(fbName)
                        self.eventStart.append(Start)
                        self.timeStart.append(timeStart)
                        self.timeEnd.append(timeEnd)
                    }
                    
                }
            }
            self.collectionView.reloadData()
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (t) in
                self.loadingView.removeFromSuperview()
            }
            
        }
        
    }
    
    func setUpLoading() {
        loadingView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        loadingView.backgroundColor = .white
        logoView.center = loadingView.center
        setUpVideo()
        self.view.addSubview(loadingView)
        self.loadingView.addSubview(logoView)
    }

    private func setUpVideo() {
        let path = URL(fileURLWithPath: Bundle.main.path(forResource: "Loading", ofType: "mp4")!)
        let player = AVPlayer(url: path)

        let newPlayer = AVPlayerLayer(player: player)
        newPlayer.frame = self.logoView.frame
        self.logoView.layer.addSublayer(newPlayer)
        newPlayer.videoGravity = AVLayerVideoGravity.resizeAspectFill

        player.play()
        player.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.videoDidPlayToEnd(_:)), name: NSNotification.Name("AVPlayerItemDidPlayToEndTimeNotification"), object: player.currentItem)

    }

    @objc func videoDidPlayToEnd(_ notification: Notification) {
        let player: AVPlayerItem = notification.object as! AVPlayerItem
        player.seek(to: CMTime.zero, completionHandler: nil)
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EventViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if eventName.count >= 1 {
            return eventName.count
        }
        else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventCell", for: indexPath) as! EventCollectionViewCell
        cell.layer.cornerRadius = 15
        cell.dayView.layer.cornerRadius = 15
        if eventName.count != 0 {
            cell.eventnameLabel.isHidden = false
            cell.dayLabel.isHidden = false
            cell.eventTimeLabel.isHidden = false
            cell.dayView.isHidden = false
            cell.eventnameLabel.text = eventName[indexPath.item]
            cell.dayLabel.text = eventStart[indexPath.item]
            cell.eventTimeLabel.text = "\(timeStart[indexPath.item]) - \(timeEnd[indexPath.item])"
            cell.bgImageView.image = UIImage(named: "route_bg")
        } else {
            cell.eventnameLabel.isHidden = true
            cell.dayLabel.isHidden = true
            cell.eventTimeLabel.isHidden = true
            cell.dayView.isHidden = true
            cell.contentView.backgroundColor = .clear
            cell.bgImageView.image = UIImage(named: "event_nil")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if eventName.count > 0 {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let desVC = mainStoryboard.instantiateViewController(identifier: "eventInfoVC") as! EventInfoViewController
            desVC.eventName = eventName[indexPath.item]
            //desVC.logoImage = attractionImage[indexPath.item]
            self.navigationController?.pushViewController(desVC, animated: true)
        }
        
    }
    
    
}
