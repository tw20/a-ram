//
//  ProfileViewController.swift
//  A-RAM
//
//  Created by Pakkapong Somboon on 24/1/2563 BE.
//  Copyright © 2563 Pakkapong Somboon. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: UIViewController {
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    
    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    func setUpView() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let getUsername = defaults.string(forKey: Keys.username)
        let getZodiac = defaults.string(forKey: Keys.zodiac)
        usernameLabel.text = getUsername
        profileImageView.image = UIImage(named: getZodiac!)
        headerView.bottomRoundCorners(cornerRadius: 15)
    }
    

    @IBAction func logOutTapped(_ sender: Any) {
        guard let user = Auth.auth().currentUser else { return }
        if !user.isAnonymous {
            try! Auth.auth().signOut()
            deleteDataFromUserDefaults()
        }
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let controller = storyboard.instantiateViewController(identifier: "welcomeVC")
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func settingTapped(_ sender: Any) {
        performSegue(withIdentifier: "settingSegue", sender: self)
    }
}
